## Commentaires

{{ IDE('exo_corr') }}


### Solutions alternatives

Pour le parcours sur les indices, on pourrait aussi jouer sur le pas :

```python
somme_indices = 0
for i in range(0, len(nombres), 2):
    somme_indices += nombres[i]
```

Dans ce cas, on ne lit qu'un indice sur deux en débutant à `0`.

Dans le même esprit, les tranches permettent de modifier les listes en omettant certains indices :

```python
somme_indices = 0
for x in nombres[::2]:
    somme += x
```

Dans ce cas, on indique à Python de ne prendre qu'un nombre `x` sur deux. La notation `nombres[::2]` est un raccourci pour
`nombres[0:len(nombres):2]`.

### Approche fonctionnelle

D'autre part, la fonction `#!py sum` permet d'utiliser une approche fonctionnelle :

```python
def somme_pairs(nombres):
    return (sum([nombres[i] for i in range(0, len(nombres), 2)]), sum([x for x in nombres if x % 2 == 0]))
```