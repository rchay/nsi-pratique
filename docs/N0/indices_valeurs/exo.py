def somme_pairs(nombres):
    # Somme des nombres d'indices pairs
    somme_indice = ...
    for ... in ...:
        if ...:
            somme_indice = ...

    # Somme des nombres pairs
    somme_nombres = ...
    for ...:
        ...


    return (somme_indice, somme_nombres)

# Tests
assert somme_pairs([]) == (0, 0)
assert somme_pairs([4, 6, 3]) == (7, 10)