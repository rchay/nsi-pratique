# Tests
assert est_bissextile(2022) == False
assert est_bissextile(2020) == True
assert est_bissextile(2100) == False
assert est_bissextile(2400) == True

# Tests supplémentaires
for annee in range(1001):
    assert est_bissextile(annee) == (
        annee % 400 == 0 or (annee % 4 == 0 and annee % 100 != 0)
    )
