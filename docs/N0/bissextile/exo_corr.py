def est_bissextile(annee):
    if annee % 400 == 0:
        return True
    elif annee % 100 == 0:
        return False
    elif annee % 4 == 0:
        return True
    else:
        return False


# Tests
assert est_bissextile(2022) == False
assert est_bissextile(2020) == True
assert est_bissextile(2100) == False
assert est_bissextile(2400) == True
