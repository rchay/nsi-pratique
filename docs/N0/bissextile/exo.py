def est_bissextile(annee):
    ...

# Tests
assert est_bissextile(2022) == False
assert est_bissextile(2020) == True
assert est_bissextile(2100) == False
assert est_bissextile(2400) == True