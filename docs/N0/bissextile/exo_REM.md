## Commentaires

Si l'on applique la règle telle qu'elle est formulée dans l'énoncé on obtient :

```python
def est_bissextile(annee):
    if annee % 4 != 0:      # année non div. par 4
        return False            
    else:                   # année div. par 4
        if annee % 100 != 0:    # année non div. par 100 
            return True
        else:                   # année div. par 100
            if annee % 400 != 0:    # année non div. par 400
                return False
            else:                   # année div. par 400 
                return True
```

Débuter avec les grandes valeurs renverse le schéma :

```mermaid
graph LR
    A{annee est div. par 400 ?} -->|Oui| B[Bissextile]
    A --->|Non| C{annee est div. par 100 ?}
    C -->|Oui| D[Non bissextile]
    C ---> |Non| E{annee est div. par 4 ?}
    E --> |Oui| F[Bissextile]
    E --> |Non| G[Non bissextile]
```

Elle permet toutefois d'avoir un code plus lisible :

{{ IDE('exo_corr') }}

On peut aussi répondre en une ligne :

```python
def est_bissextile(annee):
    return annee % 400 == 0 or (annee % 4 == 0 and annee % 100 != 0)
```