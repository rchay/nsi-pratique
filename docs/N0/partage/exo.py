def partage(valeurs, effectif_debut):
    taille = len(valeurs)

    debut_de_valeurs = []
    for i in range(...):
        ....append(valeurs[i])

    fin_de_valeurs = []
    for i in range(..., ...):
        ....append(...)

    return (..., ...)


# Tests
assert partage(["pim", "pam", "poum"], 2) == (["pim", "pam"], ["poum"])
assert partage([7, 12, 5, 6, 8], 0) == ([], [7, 12, 5, 6, 8])
assert partage([7, 12, 5, 6, 8], 5) == ([7, 12, 5, 6, 8], [])
