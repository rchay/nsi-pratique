def wordle(guess, solution):
    ...


# Tests

assert wordle("sweet", "skill") == [2, 0, 0, 0, 0]
assert wordle("apple", "banjo") == [1, 0, 0, 0, 0]
assert wordle("today", "banjo") == [0, 1, 0, 1, 0]
assert wordle("arise", "raise") == [1, 1, 2, 2, 2]
