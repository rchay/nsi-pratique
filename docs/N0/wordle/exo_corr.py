def wordle(guess, solution):
    result = []
    for i in range(len(guess)):
        if guess[i] == solution[i]:
            l = 2
        elif guess[i] in solution:
            l = 1
        else:
            l = 0
        result.append(l)
    return result


# Tests

assert wordle("sweet", "skill") == [2, 0, 0, 0, 0]
assert wordle("apple", "banjo") == [1, 0, 0, 0, 0]
assert wordle("today", "banjo") == [0, 1, 0, 1, 0]
assert wordle("arise", "raise") == [1, 1, 2, 2, 2]
