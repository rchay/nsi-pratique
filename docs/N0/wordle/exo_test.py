# Tests

assert wordle("sweet", "skill") == [2, 0, 0, 0, 0]
assert wordle("apple", "banjo") == [1, 0, 0, 0, 0]
assert wordle("today", "banjo") == [0, 1, 0, 1, 0]
assert wordle("arise", "raise") == [1, 1, 2, 2, 2]

# Tests supplémentaires

assert wordle("break", "break") == [2, 2, 2, 2, 2]
assert wordle("brine", "saint") == [0, 0, 2, 2, 0]
assert wordle("rupee", "watch") == [0, 0, 0, 0, 0]
assert wordle("aaaaa", "bbbbb") == [0, 0, 0, 0, 0]
assert wordle("baaaa", "aaaab") == [1, 2, 2, 2, 1]
