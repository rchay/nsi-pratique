---
author: Raphael Chay
title: Wordle
tags:
  - 1-boucle
  - 2-string
---

# Wordle

Wordle est un jeu de lettres où le joueur doit deviner un mot inconnu. A chaque tour, le joueur entre un mot et le jeu lui indique si chaque lettre est bien placée (affichée en vert), présente dans le mot mais à un emplacement différent (affichée en jaune) ou absente du mot (affichée en gris). Voici un exemple d'une partie où le mot à deviner est `REBUS` :

![exemple](wordle_196_example.svg)

Compléter la fonction `wordle` qui simule un tour de jeu. Les paramètres de la fonction sont :

- `guess` : une chaîne de caractères représentant le mot entré par le joueur
- `solution` : une chaîne de caractères représentant le mot à deviner

La fonction renvoie une liste d'entiers où chaque entier représente l'état d'une des lettres du paramètre `guess`. La valeur de l'entier est :

- 2 si la lettre est correctement devinée
- 1 si la lettre est présente dans `solution`
- 0 si la lettre n'est pas présente dans `solution`

!!! example "Exemples"

    ```pycon
    >>> wordle("sweet", "skill")
    [2, 0, 0, 0, 0]
    >>> wordle("apple", "banjo")
    [1, 0, 0, 0, 0]
    >>> wordle("today", "banjo")
    [0, 1, 0, 1, 0]
    >>> wordle("arise", "raise")
    [1, 1, 2, 2, 2]
    ```

{{ IDE('exo') }}
