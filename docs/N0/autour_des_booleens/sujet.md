---
author: Nicolas Revéret
title: Autour des booléens
tags:
  - 2-booléen
---
# Autour des booléens

## Présentation

Le type `bool` de Python ne contient que deux valeurs : `True` et `False`. Ces valeurs permettent répondre à des questions du type Vrai/Faux.

Un objet de type `bool` est donc une *valeur booléenne*. Ce nom est un hommage au mathématicien anglais George Boole (1815 - 1864).

On donne ci-dessous des exemples d'expressions renvoyant des booléens :

???+ example "Exemples"

    * Comparaisons :

    ```pycon
    >>> test = (1 + 1 == 2) # test d'égalité
    >>> test
    True
    >>> test = (1 + 1 != 3) # test de différence
    >>> test
    True
    >>> test = (1 >= 1)
    >>> test
    True
    >>> test = ('allo' < 'bonjour')
    >>> test
    True
    ```

    * Divisibilité de nombres :

    ```pycon
    >>> # 6 est-il pair ?
    >>> # 6 est-il divisible par 2 ?
    >>> # Le reste de la division de 6 par 2 vaut-il 0 ?
    >>> test = (6 % 2 == 0)
    >>> test
    True
    >>> # 6 est-il impair ?
    >>> # Le reste de la division de 6 par 2 vaut-il 1 ?
    >>> test = (6 % 2 == 1)
    >>> test
    False
    ```

    * Appartenance à une chaîne de caractère, une liste... :

    ```pycon
    >>> test = ('a' in 'allo')
    >>> test
    True
    >>> test = (5 not in [3, 6, 9, 12, 15])
    True
    ```

    * Opérateurs booléens

    ```pycon
    >>> test = ( ('a' in 'allo') and (1 + 1 == 3) )
    >>> test
    False
    >>> test = ( ('b' in 'allo') or (1 + 1 == 3) )
    >>> test
    False
    ```

    *Remarque :* Les parenthèses autour de chaque test ne sont pas toujours nécessaires. On les ajoute ici pour des raisons de lisibilité.

## Au travail

Compléter le code ci-dessous en saisissant les tests demandés **et non leur résultat**.

{{ IDE('exo') }}