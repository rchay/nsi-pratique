# Exemple donné de traduction de condition
# 3 est-il strictement inférieur à 4 ?
test_1 = (3 < 4)

# À vous de traduire la condition
# 3 est-il supérieur ou égal à 4 ?
test_2 = ...

# le caractère 'n' est-il dans la chaîne 'bonjour' ?
test_3 = ...

# Le calcul 3 + 5 * 2 vaut-il 16 ?
test_4 = ...

# 5 est-il un nombre pair ?
test_5 = ...

# 12 est-il dans la liste [k for k in range(12)] ?
test_6 = ... [k for k in range(12)]

# 'ju' n'est-il pas 'bonjour' ?
test_7 = ...

# Est-ce que 3 est égal à 1 + 2 et 'a' est dans 'Boole' ?
test_8 = ...

# Est-ce que 3 est égal à 1 + 2 ou 'a' est dans 'Boole' ?
test_9 = ...

# 6 est-il un nombre pair et un multiple de 3 ?
test_10 = ...

# 648 est-il dans la table de 2, de 3 et de 4 ?
test_11 = ...

# 25 est-il compris entre 24 et 26 (au sens strict) ?
test_12 = ...

# 'a' est-il dans 'hello' ou dans 'hallo' ?
test_13 = ...

# 8 est-il égal à 4 * 2 et différent de 9 - 1 ?
test_14 = ...

# 7 est-il compris entre 1 et 5 (au sens large) ou strictement supérieur à 6 ?
test_15 = ...
