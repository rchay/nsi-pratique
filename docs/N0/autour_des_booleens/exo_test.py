# Tests
# 3 est-il strictement inférieur à 4 ?
assert test_1
# 3 est-il supérieur ou égal à 4 ?
assert not test_2
# le caractère 'n' est-il dans la chaîne 'bonjour' ?
assert test_3
# Le calcul 3 + 5 * 2 vaut-il 16 ?
assert not test_4
# 5 est-il un nombre pair ?
assert not test_5
# 12 est-il dans la liste [k for k in range(12)] ?
assert not test_6
# 'ju' n'est-il pas 'bonjour' ?
assert test_7
# Est-ce que 3 est égal à 1 + 2 et 'a' est dans 'Boole' ?
assert not test_8
# Est-ce que 3 est égal à 1 + 2 ou 'a' est dans 'Boole' ?
assert test_9
# 6 est-il un nombre pair et un multiple de 3 ?
assert test_10
# 648 est-il dans la table de 2, de 3 et de 4 ?
assert test_11
# 25 est-il compris entre 24 et 26 (au sens strict) ?
assert test_12
# 'a' est-il dans 'hello' ou dans 'hallo' ?
assert test_13
# 8 est-il égal à 4 * 2 et différent de 9 - 1 ?
assert not test_14
# 7 est-il compris entre 1 et 5 (au sens large) ou strictement supérieur à 6 ?
assert test_15
