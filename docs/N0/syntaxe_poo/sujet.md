---
author: Nicolas Revéret
title: Syntaxe de la POO
tags:
  - 7-POO
---

# `programmeur.saisit(code)`

On travaille dans cet exercice sur deux **classes** :

* la classe `Personnage` permet de représenter un personnage d'un jeu vidéo,
* la classe `Chose` permet de décrire une chose (objet, élément du décor...) dans le jeu.

```mermaid
classDiagram
class Personnage{
       nom : str
       energie : int
       inventaire : list
       prend(chose : Chose) str
       mange(chose : Chose) str
       parle(autre : Personnage) str
       donne(chose : Chose, autre : Personnage) str
}

class Chose{
       genre : str
}
```

Un *objet* de la classe `Personnage` est caractérisé par :

* des **attributs** :
    * un nom `nom`(au format `str`),
    * une énergie `energie` (au format `int`, compris entre `0` et `500`),
    * un inventaire `inventaire` (au format `list`),
* des **méthodes** :
    * `prend(self, chose)` : ajoute, si c'est possible, `chose` (de type `Chose`) dans l'inventaire du personnage,
    * `mange(self, chose)` : le personnage mange, si c'est possible, la chose (de type `Chose`),
    * `parle(self, autre)` : le personnage et `autre` (du type `Personnage`) discutent,
    * `donne(self, chose, personne)` : le personnage donne la chose (de type `Chose`) à l'autre personne (de type `Personnage`).

Toutes ces méthodes renvoient des chaînes de caractères décrivant le résultat de l'action.

Un objet de la classe `Chose` est quant à lui simplement caractérisé par son genre `genre` de type `str`. Il n'est possible de créer que certains genre de choses.

???+ tip "Syntaxe"

    Il est possible de créer une `Chose` en saisissant l'instruction `truc = Chose("pierre")`.

    De plus, si `untel` et `unetelle` sont des objets de type `Personnage` et `truc` un objet de type `Chose`, il par exemple est possible :

    * d'accéder à la valeur d'un attribut en saisissant `invtr = untel.inventaire`,
  
    * de modifier la valeur d'un attribut en saisissant `unetelle.energie = 125`,
      
    * d'utiliser une méthode en saisissant `untel.donne(truc, unetelle)`. Il est inutile et **incorrect** d'écrire le paramètre `self` donné dans la définition.

Compléter le code ci-dessous avec les instructions valides :

{{ IDE('exo') }}