## Commentaires

{{ IDE('exo_corr') }}

On pourrait aussi utiliser une liste en compréhension:

```python
def liste_longueur(liste_prenoms, longueur):
    return [prenom for prenom in liste_prenoms if len(prenom) == longueur]
```
