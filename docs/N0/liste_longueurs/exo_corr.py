def liste_longueur(liste_prenoms, longueur):
    prenoms_longueur = []
    for prenom in liste_prenoms:
        if len(prenom) == longueur:
            prenoms_longueur.append(prenom)
    return prenoms_longueur


# Tests
prenoms = ["Anne", "Francky", "Charles", "Léa", "Nicolas"]
assert liste_longueur(prenoms, 7) == ["Francky", "Charles", "Nicolas"]
assert liste_longueur(prenoms, 3) == ["Léa"]
assert liste_longueur(prenoms, 10) == []
