---
author: Charles Poulmaire, Franck Chambon, Nicolas Revéret
title: Liste de prénoms
tags:
  - 1-boucle
---

# Liste des longueurs

Compléter la fonction `liste_longueur` afin qu'elle renvoie la liste des prénoms de la longueur souhaitée.

Cette fonction :

* prend deux arguments : `liste_prenoms`, une liste de prénoms et `longueur`, un entier naturel ;
* renvoie la liste des prénoms de `liste_prenoms` dont la longueur est égale à `longueur`.

On prendra soin de renvoyer les prénoms dans **leur ordre d'apparition** dans la liste initiale.

!!! example "Exemples"

    ```pycon
	>>> liste_longueur(['Anne', 'Francky', 'Charles', 'Léa', 'Nicolas'], 7)
    ['Francky', 'Charles', 'Nicolas']
	>>> liste_longueur(['Anne', 'Francky', 'Charles', 'Léa', 'Nicolas'], 3)
    ['Léa'] 
	>>> liste_longueur(['Anne', 'Francky', 'Charles', 'Léa', 'Nicolas'], 10)
    [] 
    ```

{{ IDE('exo') }}