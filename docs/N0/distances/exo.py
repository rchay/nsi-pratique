def manhattan(..., ..., ..., ...):
    return ...


def euclidienne_carre(..., ..., ..., ...):
    ...


def ...:
    ...


# Tests
xA, yA = 2, -5
xB, yB = -3, 1
assert manhattan(xA, yA, xB, yB) == 11
assert euclidienne_carre(xA, yA, xB, yB) == 61
assert tchebychev(xA, yA, xB, yB) == 6

xA, yA = 2, 2
xB, yB = 2, 2
assert manhattan(xA, yA, xB, yB) == 0
assert euclidienne_carre(xA, yA, xB, yB) == 0
assert tchebychev(xA, yA, xB, yB) == 0
