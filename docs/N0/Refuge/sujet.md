---
author: charles Poulmaire
title: Refuge d'animaux
---

# Liste dictionnaire

Le refuge Wouaf_Miaou s'occupe de chats et de chiens pouvant être adoptés.
Compléter la fonction `selection_enclos` afin de renvoyer une liste de noms d'animaux d'un même enclos.

!!! example "Exemples"

	```pycon
	>>> Wouaf_Miaou = [{'nom':'Medor', 'espece':'chien', 'age':5, 'enclos':2},
           {'nom':'Titine', 'espece':'chat', 'age':2, 'enclos':5},
           {'nom':'Tom', 'espece':'chat', 'age':7, 'enclos':4},
           {'nom':'Belle', 'espece':'chien', 'age':6, 'enclos':3},
           {'nom':'Mirza', 'espece':'chat', 'age':6, 'enclos':5}]
	>>> selection_enclos(refuge_mon_ami, 5)
    ['Titine', 'Mirza']
	>>> selection_enclos(refuge_mon_ami, 2)
	['Medor']
	>>> selection_enclos(refuge_mon_ami, 7)
    [] 
    ```
	