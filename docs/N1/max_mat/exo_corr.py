def ajoute_bord(tab):
    nb_l, nb_c = len(tab), len(tab[0])
    for i in range(nb_l):
        som_i = 0
        for j in range(nb_c):
            som_i += tab[i][j]
        tab[i].append(som_i)

tab1 = [
    [0, 1, 1, 1, 0, 1],
    [0, 0, 1, 0, 0, 1],
    [1, 0, 0, 1, 1, 0],
    [1, 0, 0, 1, 1, 1]
    ]


tab2 = [
    [0, 0, 0],
    [0, 0, 0]
    ]

ajoute_bord(tab1)

assert tab1 == [[0, 1, 1, 1, 0, 1, 4],
                [0, 0, 1, 0, 0, 1, 2],
                [1, 0, 0, 1, 1, 0, 3],
                [1, 0, 0, 1, 1, 1, 4]]

ajoute_bord(tab2)

assert tab2 == [[0, 0, 0, 0], [0, 0, 0, 0]]
