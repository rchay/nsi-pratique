def ote_zeros(bins):
    n = len(bins)
    assert n >= 1, "La liste ne peut être vide"
    fin = n - 1
    while bins[fin] == 0 and fin > 0:
        fin -= 1
    return bins[:fin + 1]

