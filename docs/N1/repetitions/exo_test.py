# Tests

assert nb_repetitions(5, [2, 5, 3, 5, 6, 9, 5]) == 3
assert nb_repetitions('A', ['B', 'A', 'B', 'A', 'R']) == 2
assert nb_repetitions(12, [1, 7, 21, 36, 44]) == 0
assert nb_repetitions(12, []) == 0

# Autres tests

import random

for i in range(5):
    element = random.randint(-10, 10)
    valeurs = [random.randint(-10, 10) for _ in range(1000)]
    assert nb_repetitions(element, valeurs) == valeurs.count(element), f"caché n°{i+1}"
