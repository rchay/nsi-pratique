def nb_repetitions(element, valeurs):
    nb = 0
    for elt in valeurs:
        if elt == element:
            nb = nb + 1
    return nb


assert nb_repetitions(5, [2, 5, 3, 5, 6, 9, 5]) == 3
assert nb_repetitions('A', ['B', 'A', 'B', 'A', 'R']) == 2
assert nb_repetitions(12, [1, 7, 21, 36, 44]) == 0
assert nb_repetitions(12, []) == 0

