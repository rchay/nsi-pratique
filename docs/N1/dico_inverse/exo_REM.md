# Commentaires

{{ IDE('exo_corr') }}

On peut parcourir le dictionnaire à l'aide de la méthode `#!py dico.items()`. On récupère ainsi à chaque itération le couple `cle, valeur`.

Si `valeur` n'est pas présent parmi les clés de `dico_inverse` on crée une nouvelle entrée dans le dictionnaire.

Dans le cas contraire, on se contente d'ajouter `cle` à la liste `dico_inverse[valeur]`.


## Variante sans `items()`

On peut aussi parcourir un dictionnaire avec `#!py for cle in dico:`

Ici, le code deviendrait

```python
def inverser(dico):
    dico_inverse = {}
    for cle in dico:
        valeur = dico[cle]
        if valeur not in dico_inverse:
            dico_inverse[valeur] = [cle]
        else:
            dico_inverse[valeur].append(cle)
    return dico_inverse
```
