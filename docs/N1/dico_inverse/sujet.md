---
author: Jean Diraison
title: Dictionnaire inversé
tags:
  - dictionnaire
  - boucle
---
# Dictionnaire inversé

Un dictionnaire associe des valeurs à des clés, comme par exemple `{"Paris": "Tour Eiffel", "Rome": "Colisée", "Berlin": "Reichtag", "Londres": "Big Ben"}` qui associe la valeur `"Tour Eiffel"` à la clé `"Paris"`.

```mermaid
flowchart LR
  subgraph Clés
  P[Paris]
  R[Rome]
  B[Berlin] 
  L[Londres]
  end
  subgraph Valeurs
  TE(Tour Eiffel)
  BB(Big Ben)
  RE(Reichtag)
  CO(Colisée)
  end
  P --> TE
  R --> CO
  B --> RE
  L --> BB
```

On cherche dans cet exercice à « inverser » les dictionnaires c'est à dire à construire un dictionnaire dans lequel les associations sont dans l'autre sens. Dans le cas précédent on aurait :

```mermaid
flowchart LR
  subgraph Clés
  TE[Tour Eiffel]
  BB[Big Ben]
  RE[Reichtag]
  CO[Colisée]
  end
  subgraph Valeurs
  P(Paris)
  R(Rome)
  B(Berlin)
  L(Londres)
  end
  TE --> P
  CO --> R
  RE --> B
  BB --> L
```

Dans certains cas, l'inversion simple n'est pas possible. C'est le cas par exemple du dictionnaire  `{"Paris": "P", "Lyon": "L", "Nantes": "N", "Lille": "L"}` puisque la valeur `"L"` est associée à la fois à la clé `"Lyon"` et à la clé `"Lille"`.

```mermaid
flowchart LR
  subgraph Clés
  Ly[Lyon]
  Pa[Paris]
  Na[Nantes] 
  Li[Lille]
  end
  subgraph Valeurs
  P(P)
  L(L)
  N(N)
  end
  Ly --> L
  Pa --> P
  Na --> N
  Li --> L
```

On considère donc que **le dictionnaire *inverse* associe à chaque valeur du dictionnaire initial la liste des clés associées**. 

!!! note "Remarque"

    On ne demande pas à ce que ces listes soient triées dans un ordre particulier.
    
    On triera le dictionnaire inverse dans les tests afin de gérer les différents résultats liés à des ordres différents. On garantit que toutes les clés et les valeurs utilisées sont de types comparables.

    ```python
    def trier_valeurs(dico):
        for cle, valeur in dico.items():
            dico[cle] = sorted(valeur)
        return dico
    ```

Ainsi, le dictionnaire inverse de `{"Paris": "Tour Eiffel", "Rome": "Colisée", "Berlin": "Reichtag", "Londres": "Big Ben"}` est `{'Tour Eiffel': ['Paris'], 'Colisée': ['Rome'], 'Reichtag': ['Berlin'], 'Big Ben': ['Londres']}`.

Pour `{"Paris": "P", "Lyon": "L", "Nantes": "N", "Lille": "L"}` on peut obtenir `{"P": ["Paris"], "L": ["Lyon", "Lille"], "N": ["Nantes"]}`.

Vous devez écrire une fonction `inverser` de paramètre `dico` qui renvoie le dictionnaire inversé de `dico`.

!!! example "Exemples"

    ```pycon
    >>> inverser({'a': 5, 'b': 7})
    {5: ['a'], 7: ['b']}
    >>> inverser({'a': 5, 'b': 7, 'c': 5})
    {5: ['a', 'c'], 7: ['b']}
    >>> inverser({"Paris": "Tour Eiffel", "Rome": "Colisée", "Berlin": "Reichtag", "Londres": "Big Ben"})
    {'Tour Eiffel': ['Paris'], 'Colisée': ['Rome'], 'Reichtag': ['Berlin'], 'Big Ben': ['Londres']}
    >>> inverser({"Paris": "P", "Lyon": "L", "Nantes": "N", "Lille": "L"})
    {"P": ["Paris"], "L": ["Lyon", "Lille"], "N": ["Nantes"]}
    ```

{{ IDE('exo') }}
