---
author: Nicolas Revéret
title: Mise en boîtes
tags:
    - liste
status: relecture
---

# Mise en boîte

> On n'utilisera pas `sum` dans cet exercice.

On souhaite ranger différents objets dont le poids est connu dans des boîtes dont la capacité totale ne peut pas dépasser une certaine valeur.

Tous les poids sont exprimés en kilogrammes.

Toutes les boîtes ont la même contenance et l'on garantit que celle-ci est supérieure ou égale au poids de l'objet le plus lourd.

Par exemple :

```python
poids_objets = [5, 3, 7]
poids_max = 8
```

Dans cet exemple, on a trois objets à ranger (de poids 5 kg, 3 kg et 7 kg) et les boîtes ne peuvent pas peser plus de 8 kg (inclus).

Un rangement valide dans cet exemple est `#!py [[5, 3], [7]]`. Cela signifie que l'on a placé les objets de poids 5 kg et 3 kg dans la même boîte et l'objet de poids 7 kg dans une boîte distincte.

Comme on peut le voir, un rangement est donc une liste de listes Python. Chaque sous-liste représente une boîte et contient les poids des objets placés dedans. Ainsi, `#!py [[1, 4], [3, 1], [5]]` est un rangement effectué à l'aide de trois boîtes.

Pour ranger les objets, on **impose** la méthode suivante :

* on crée une liste Python contenant autant de "boîtes vides" qu'il y a d'objets
* on prend les objets dans l'ordre dans lequel ils sont fournis dans la liste
* pour chacun d'entre eux, on parcourt la liste des boîtes dans l'ordre des indices croissants et on place l'objet **dans la première boîte pouvant le recevoir** sans que son poids ne dépasse la contenance
* on renvoie la liste des boîtes non-vides

!!! tip "Coup de pouce"
    On pourra créer la liste des boîtes vides en faisant `#!py [[] for objet in poids_objets]`

Vous devez écrire deux fonctions :

* une fonction `poids_boite` prenant en argument une liste de nombres entiers (une boîte) et renvoyant la somme de ces nombres (le poids de la boîte)
* une fonction `rangement` prenant en arguments la liste des poids des objets ainsi que le poids maximal d'une boîte et renvoyant la liste **non triée** des boîtes utilisées en appliquant la méthode décrite

!!! attention "Attention"

    Comme indiqué ci-dessus, **on ne triera ni les objets ni les boîtes** au risque de faire échouer les tests

!!! example "Exemples"

    ```pycon
    >>> poids_boite([])
    0
    >>> poids_boite([5, 3, 7])
    15
    >>> ranger([5, 3, 7], 8)
    [[5, 3], [7]]
    >>> ranger([5, 3, 7], 15)
    [[5, 3, 7]]
    >>> ranger([3, 4, 9, 3], 10)
    [[3, 4, 3], [9]]
    ```

{{ IDE('exo') }}
