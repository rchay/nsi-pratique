# Tests
assert poids_boite([]) == 0
assert poids_boite([5, 3, 7]) == 15
assert ranger([5, 3, 7], 8) == [[5, 3], [7]]
assert ranger([5, 3, 7], 15) == [[5, 3, 7]]
assert ranger([3, 4, 9, 3], 10) == [[3, 4, 3], [9]]

# Tests supplémentaires
assert poids_boite([-1, 1]) == 0
assert poids_boite([-1, -1]) == -2
assert ranger([10, 10, 10], 10) == [[10], [10], [10]]
assert ranger([0, 0, 0], 15) == [[0, 0, 0]]
assert ranger([9, 1, 9, 1], 10) == [[9, 1], [9, 1]]
assert ranger([9, 9, 9, 1], 10) == [[9, 1], [9], [9]]
