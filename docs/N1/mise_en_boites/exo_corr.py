def poids_boite(boite):
    total = 0
    for objet in boite:
        total += objet

    return total


def ranger(poids_objets, poids_max):
    liste_boites = [[] for objet in poids_objets]

    for objet in poids_objets:
        indice_boite = 0
        while poids_boite(liste_boites[indice_boite]) + objet > poids_max:
            indice_boite += 1
        liste_boites[indice_boite].append(objet)

    return [boite for boite in liste_boites if len(boite) > 0]


# Tests
assert poids_boite([]) == 0
assert poids_boite([5, 3, 7]) == 15
assert ranger([5, 3, 7], 8) == [[5, 3], [7]]
assert ranger([5, 3, 7], 15) == [[5, 3, 7]]
assert ranger([3, 4, 9, 3], 10) == [[3, 4, 3], [9]]

# Tests supplémentaires
assert poids_boite([-1, 1]) == 0
assert poids_boite([-1, -1]) == -2
assert ranger([10, 10, 10], 10) == [[10], [10], [10]]
assert ranger([0, 0, 0], 15) == [[0, 0, 0]]
assert ranger([9, 1, 9, 1], 10) == [[9, 1], [9, 1]]
assert ranger([9, 9, 9, 1], 10) == [[9, 1], [9], [9]]
