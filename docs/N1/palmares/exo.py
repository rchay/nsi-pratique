def palmares(notes):
    ...


# Tests
notes = {
    "Alice": 18,
    "Bob": 10,
    "Charles": 8,
    "David": 12,
    "Erwann": 17,
    "Fanny": 14,
    "Guillaume": 16,
    "Hugo": 14,
}
note_max, eleves = palmares(notes)
assert (note_max, sorted(eleves)) == (18, ["Alice"])

notes = {"Bob": 10, "Charles": 8, "David": 12, "Hugo": 14, "Fanny": 14}
note_max, eleves = palmares(notes)
assert (note_max, sorted(eleves)) == (14, ["Fanny", "Hugo"])

notes = {"Romuald": 15, "Ronan": 14, "David": 15}
note_max, eleves = palmares(notes)
assert (note_max, sorted(eleves)) == (15, ["David", "Romuald"])
