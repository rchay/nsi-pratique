def tri_3_valeurs(nombres):
    i0 = 0
    i1 = 0
    i2 = len(nombres) - 1

    while i1 <= i2:
        if nombres[i1] == 0:
            nombres[i1], nombres[i0] = nombres[i0], nombres[i1]
            i0 += 1
            i1 += 1
        elif nombres[i1] == 1:
            i1 += 1
        else:
            nombres[i1], nombres[i2] = nombres[i2], nombres[i1]
            i2 -= 1


# Tests
nombres = [2, 0, 2, 1, 2, 1]
tri_3_valeurs(nombres)
assert nombres == [0, 1, 1, 2, 2, 2]

nombres = [1, 1, 0, 1]
tri_3_valeurs(nombres)
assert nombres == [0, 1, 1, 1]
