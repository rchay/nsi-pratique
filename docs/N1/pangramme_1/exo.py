def est_pangramme(phrase):
    ...


def indice(minuscule):
    return ord(minuscule) - ord("a")


# Tests
assert est_pangramme("portez ce vieux whisky au juge blond qui fume !") == True
assert est_pangramme("portez un vieux whisky au juge blond qui fume !") == False
assert est_pangramme("jugez que ce texte renferme l'alphabet, dix voyelles, k et w") == True
assert est_pangramme("jugez que ce texte renferme l'alphabet, dix voyelles et w") == False
