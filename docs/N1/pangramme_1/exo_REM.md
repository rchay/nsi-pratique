# Commentaires

{{ IDE('exo_corr') }}

On crée tout d'abord la liste des booléens indicateurs de la présence de chaque lettre dans la phrase.

On parcourt ensuite la chaîne de caractères. Pour chaque caractère on bascule la valeur du booléens à `#!py True`.

La chaîne est donc un pangramme si, en fin de boucle, la liste `presences` ne contient que des `#!py True`. On le vérifie, ici, à l'aide d'une boucle.

On pourrait aussi utiliser la fonction `all` qui ne renvoie `#!py True` que si tous les booléens de la liste passés en argument valent `#!py True` :

```python
def est_pangramme(phrase):
    presences = [False] * 26
    for caractere in phrase:
        if "a" <= caractere <= "z":
            presences[indice(caractere)] = True

    return all(presences)
```

## Avec un ensemble

On pourrait aussi utiliser un ensemble (hors programme NSI), `#!py set` en Python. Cette structure de données ne comporte pas de doublons.

```python
def est_pangramme(phrase):
    minuscules_presentes = set()
    for caractere in phrase:
        if "a" <= caractere <= "z":
            minuscules_presentes.add(caractere)
    return len(minuscules_presentes) == 26
```

En utilisant un ensemble en compréhension on obtient même :

```python
def est_pangramme(phrase):
    minuscules_presentes = {caractere for caractere in phrase if "a" <= caractere <= "z"}
    return len(minuscules_presentes) == 26
```