
# Tests
assert est_pangramme("portez ce vieux whisky au juge blond qui fume !") == True
assert est_pangramme("portez un vieux whisky au juge blond qui fume !") == False
assert est_pangramme("jugez que ce texte renferme l'alphabet, dix voyelles, k et w") == True
assert est_pangramme("jugez que ce texte renferme l'alphabet, dix voyelles et w") == False

# Tests supplémentaires
assert est_pangramme("the quick brown fox jumps over The lazy dog.") == True
assert (
    est_pangramme(
        "hier, au zoo, j'ai vu dix guépards, cinq zébus, un yak et le wapiti fumer"
    )
    == True
)
assert est_pangramme(";".join([chr(i) for i in range(ord("a"), ord("a") + 26)])) == True
assert (
    est_pangramme(";".join([chr(i) for i in range(ord("b"), ord("b") + 25)])) == False
)
