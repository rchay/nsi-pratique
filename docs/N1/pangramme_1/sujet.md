---
author: Nicolas Revéret
title: Pangramme
tags:
  - 2-booléen
---
# Reconnaître un pangramme

Un *pangramme* est une phrase contenant toutes les lettres de l'alphabet.

Il existe dans l'alphabet latin utilisé en français 26 lettre minuscules « usuelles ». Si l'on tient compte des lettres accentuées et des ligatures (*à*, *â*, ..., *ç*, *æ* et *œ*), le décompte passe à 42 caractères minuscules distincts.

**On se limite dans cet exercice aux lettres minuscules usuelles de l'alphabet : *a*, *b*, *c*, ..., *z*.**

Un exemple classique de pangramme est « *portez ce vieux whisky au juge blond qui fume* ». Chaque lettre y apparaît au moins une fois.

Par contre « *portez __un__ vieux whisky au juge blond qui fume* » n'est pas un pangramme ; il manque la lettre *c*.

Vous devez écrire une fonction `est_pangramme` qui :

* prend en argument une chaîne de caractères `phrase`,
* renvoie le booléen indiquant si cette chaîne est un pangramme.

!!! example "Exemples"

    ```pycon
    >>> est_pangramme("portez ce vieux whisky au juge blond qui fume !")
    True
    >>> est_pangramme("portez un vieux whisky au juge blond qui fume !")
    False
    >>> est_pangramme("jugez que ce texte renferme l’alphabet, dix voyelles, k et w")
    True
    >>> est_pangramme("jugez que ce texte renferme l’alphabet, dix voyelles et w")
    False
    ```

On fournit la fonction `indice` qui prend en argument une lettre entre `"a"` et `"z"` et renvoie son indice dans l'alphabet (en débutant à `0` pour `"a"`).

```python
def indice(minuscule):
    return ord(minuscule) - ord("a")
```

On pourra utiliser une liste `presences` contenant initialement 26 fois `#!py False` et mise à jour au fil des apparitions de chaque lettre dans la phrase.

{{ IDE('exo') }}
