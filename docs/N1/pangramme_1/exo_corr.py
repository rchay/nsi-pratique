def est_pangramme(phrase):
    presences = [False] * 26
    for caractere in phrase:
        if "a" <= caractere <= "z":
            presences[indice(caractere)] = True

    for i in range(26):
        if not presences[i]:
            return False

    return True


def indice(minuscule):
    return ord(minuscule) - ord("a")


# Tests
assert est_pangramme("portez ce vieux whisky au juge blond qui fume !") == True
assert est_pangramme("portez un vieux whisky au juge blond qui fume !") == False
assert (
    est_pangramme("jugez que ce texte renferme l'alphabet, dix voyelles, k et w")
    == True
)
assert (
    est_pangramme("jugez que ce texte renferme l'alphabet, dix voyelles et w") == False
)
