
# Tests
nombres = [3, 0, 2, 1, 5, 1]
tri_pivot(nombres, 1)
assert nombres == [0, 1, 1, 5, 2, 3]

nombres = [7, 2, 5, 5, 9, 1, 9, 7, 2, 5]
tri_pivot(nombres, 7)
assert nombres == [2, 5, 5, 5, 1, 2, 7, 7, 9, 9]

