## Commentaires

{{ IDE('exo_corr') }}

Dans le cas où le tableau ne comporte que trois valeurs distinctes, on retrouve le problème dit du [*Drapeau Hollandais*](https://fr.wikipedia.org/wiki/Probl%C3%A8me_du_drapeau_hollandais) inventé par E. Dijkstra.

L'adaptation à un nombre quelconque de valeurs présentée ici peut-être utilisée dans le cadre du « tri rapide ». Dans cet algorithme, on trie récursivement les valeurs autour d'une valeur pivot comme dans l'exercice.

On peut toutefois montrer que lorsque le tableau à trier comporte peu de valeurs distinctes (relativement à sa taille), la méthode proposée ici est plus intéressante que la méthode classique. L'avantage ici est en effet d'organiser le tableau autour du pivot mais aussi **de regrouper les valeurs égales au pivot** au centre du tableau.