Variante : on peut demander d'afficher des "barres" horizontales.


```python
def histo(liste_ent):
    if len(liste_ent) == 0:
        return []
    n = max(liste_ent)
    compteurs = [0 for _ in range(n + 1)]
    for entier in liste_ent:
        compteurs[entier] += 1
    for i in range(len(compteurs)):
        print(str(i) + " : " + '*'*compteurs[i])
		# print(f"{i} : {'*'*compteurs[i]}")
```

qui donne avec `[1, 1, 1, 3, 2, 1, 2 ,5]` :

```python
0 : 
1 : ****
2 : **
3 : *
4 : 
5 : *
```
