---
author: Guillaume CONNAN
title: Histogramme

---

# Histogramme

On dispose d'une liste d'entiers naturels entre 0 et 9 `liste_ent`. 
On demande de créer une fonction `histo` qui renvoie une liste dont les éléments
sont le nombre d'occurrences de leur indice dans `liste_ent`.

!!! example "Exemple"

	```pycon
	>>> liste_ent = [1, 1, 1, 3, 2]
	>>> histo(liste_ent)
	[0, 3, 1, 1, 0, 0, 0, 0, 0, 0]
	```

En effet, 


- La première valeur de la liste renvoyée est 0, car la valeur 0 n'apparaît pas dans [1, 1, 1, 3, 2],
- la seconde valeur est 1, la valeur 1 apparaissant trois fois dans la liste [1, 1, 1, 3, 2],
- ensuite la valeur est 1, car la valeur 2 apparait une fois dans la liste [1, 1, 1, 3, 2],
- ensuite la valeur est 1, car la valeur 3 n'apparait une fois,
-  enfin,  il  n'y  a  que  des  0  ensuite  car  les  nombres  supérieurs  à  3
  n'apparaissent pas.




!!! example "Autres exemples"

	```pycon
	>>> liste_ent = [1, 1, 1, 3, 2, 1, 2 ,5]
	>>> histo(liste_ent)
	[0, 4, 2, 1, 0, 1, 0, 0, 0, 0, 0]
	>>> histo([5, 5, 5, 0, 7])
	[1, 0, 0, 0, 0, 3, 0, 1, 0, 0, 0]]
	>>> histo([])
	[0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
	```

{{ IDE('exo') }}
