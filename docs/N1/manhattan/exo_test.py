# Tests
assert distance(0, 1, 2, 3) == 4
adresses = [(2, 0), (0, 1), (3, 3), (2, 3)]
assert prochaine_livraison(adresses, 0, 0) == (0, 1)
adresses = [(2, 0), (3, 3), (2, 3)]
assert prochaine_livraison(adresses, 0, 1) == (2, 0)
adresses = [(3, 3), (2, 3)]
assert prochaine_livraison(adresses, 2, 0) == (2, 3)
adresses = [(3, 3)]
assert prochaine_livraison(adresses, 2, 3) == (3, 3)
adresses = [(1, 0), (0, 1)]
assert prochaine_livraison(adresses, 0, 0) == (1, 0)

# Autres tests
# Une livraison
assert prochaine_livraison(adresses, 0, 0) == (1, 0)
# Sur place
adresses = [(1, 0)]
assert prochaine_livraison(adresses, 1, 0) == (1, 0)
# Au milieu
adresses = [(0, 0), (2, 0)]
assert prochaine_livraison(adresses, 1, 0) == (0, 0)
# Dans les négatifs
adresses = [(-1, -1), (-3, 0)]
assert prochaine_livraison(adresses, 0, 0) == (-1, -1)
