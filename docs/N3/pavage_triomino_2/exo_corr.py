#--- HDR ---#
def est_pavage(n, i_trou, j_trou, triominos):
    if 3 * len(triominos) + 1 != n * n:
        return False
    pavage = [[False for j in range(n)] for i in range(n)]
    pavage[i_trou][j_trou] = True
    for (i, j, sens) in triominos:
        if not(0 <= i < n) or not(0 <= j < n):
            return False
        # carré central
        if pavage[i][j]:
            return False
        else:
            pavage[i][j] = True
        # en haut, ou en bas
        if sens > 1:  # en bas
            if i + 1 >= n:
                return False
            if pavage[i + 1][j]:
                return False
            pavage[i + 1][j] = True
        else:         # en haut
            if i - 1 < 0:
                return False
            if pavage[i - 1][j]:
                return False
            pavage[i - 1][j] = True
        # à gauche, ou à droite
        if sens % 2 == 1:  # à droite
            if j + 1 >= n:
                return False
            if pavage[i][j + 1]:
                return False
            pavage[i][j + 1] = True
        else:              # à gauche
            if j - 1 < 0:
                return False
            if pavage[i][j - 1]:
                return False
            pavage[i][j - 1] = True
    # vérification que tout est rempli
    # (facultatif avec le premier test)
    for i in range(n):
        for j in range(n):
            if not pavage[i][j]:
                return False
    return True
#--- HDR ---#
def pavage_triominos(n, i_trou, j_trou):
    resultat = []
    if n == 1:
        return resultat
    m = n // 2
    if i_trou < m:  # le trou est en haut
        if j_trou < m:  # le trou est à gauche
            for (i, j, s) in pavage_triominos(m, i_trou, j_trou):
                resultat.append((i, j, s))
            sens = 0
            resultat.append((m, m, sens))
        else:           # le trou est à droite
            for (i, j, s) in pavage_triominos(m, i_trou, j_trou - m):
                resultat.append((i, j + m, s))
            sens = 1
            resultat.append((m, m - 1, sens))
    else:           # le trou est en bas
        if j_trou < m:  # le trou est à gauche
            for (i, j, s) in pavage_triominos(m, i_trou - m, j_trou):
                resultat.append((i + m, j, s))
            sens = 2
            resultat.append((m - 1, m, sens))
        else:           # le trou est à droite
            for (i, j, s) in pavage_triominos(m, i_trou - m, j_trou - m):
                resultat.append((i + m, j + m, s))
            sens = 3
            resultat.append((m - 1, m - 1, sens))


    if sens != 0:
        for (i, j, s) in pavage_triominos(m, m-1, m-1):
            resultat.append((i, j, s))
    if sens != 1:
        for (i, j, s) in pavage_triominos(m, m-1, 0):
            resultat.append((i, j+m, s))
    if sens != 2:
        for (i, j, s) in pavage_triominos(m, 0, m-1):
            resultat.append((i+m, j, s))
    if sens != 3:
        for (i, j, s) in pavage_triominos(m, 0, 0):
            resultat.append((i+m, j+m, s))
    return resultat

# tests
assert est_pavage(2, 1, 1, pavage_triominos(2, 1, 1)) == True

assert est_pavage(4, 0, 1, pavage_triominos(4, 0, 1)) == True

assert est_pavage(8, 5, 4, pavage_triominos(8, 5, 4)) == True
