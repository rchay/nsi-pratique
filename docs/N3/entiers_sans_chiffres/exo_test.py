# Tests
# un + un == deux
assert addition(UN, UN) == DEUX
# trois + cinq == huit
assert addition(TROIS, DEUX) == CINQ
# zéro * cinq == zéro
assert multiplication(ZERO, CINQ) == ZERO
# deux * trois == six
assert multiplication(DEUX, TROIS) == SIX
# cinq - trois  == deux
assert soustraction(CINQ, TROIS) == DEUX
# trois - cinq -> Opération impossible
try:
    soustraction(DEUX, CINQ)
except ValueError:
    pass

# Tests supplémentaires
ZERO = tuple()
nombres = [ZERO]
for k in range(1, 20):
    nombres.append(successeur(nombres[-1]))
for i in range(10):
    a = nombres[i]
    b = nombres[i + 1]
    assert (
        addition(a, b) == nombres[2 * i + 1]
    ), f"Erreur sur l'addition de '{i}' et '{i+1}'"
    assert (
        soustraction(b, a) == nombres[1]
    ), f"Erreur sur la soustraction de '{i+1}' et '{i}'"
    try:
        soustraction(a, b)
    except ValueError:
        pass
    except:
        f"Erreur sur la soustraction de '{i}' et '{i+1}'. Il faut lever une erreur de valeur."
