# Test
HAUTEUR, LARGEUR = 4, 10
lab = labyrinthe(HAUTEUR, LARGEUR)
assert (
    dessin_console(lab)
    == """
 ___________________
|    _|  ___| |__ | |
|_|  _|__ |__  _____|
|     |____ |     | |
|_|_|_________|_|___|
"""
)

# Tests supplémentaires
HAUTEUR, LARGEUR = 4, 4
lab = labyrinthe(HAUTEUR, LARGEUR)
assert (
    dessin_console(lab)
    == """
 _______
| |  __ |
| |___| |
|    ___|
|_|_____|
"""
)

HAUTEUR, LARGEUR = 5, 5
lab = labyrinthe(HAUTEUR, LARGEUR)
assert (
    dessin_console(lab)
    == """
 _________
|__ |__  _|
|____     |
|__  _|_|_|
|  ____  _|
|___|_____|
"""
)

HAUTEUR, LARGEUR = 10, 10
lab = labyrinthe(HAUTEUR, LARGEUR)
assert (
    dessin_console(lab)
    == """
 ___________________
|  __    _| |  _|__ |
|_|  _|  ___|  _|   |
|__  _|_|__  _|__ |_|
|  __        ______ |
|_|  _|_| |_|  ____ |
| |_| | | | |___|___|
|  ____    _|__ |__ |
|__   |_|  ____     |
|  _|__ |_|  _|_| | |
|___|_________|___|_|
"""
)
