# tests

assert tri_par_tas([55, 42, 12, 73]) == [12, 42, 55, 73]

assert tri_par_tas(['bac', 'a', 'abc', 'b']) == ['a', 'abc', 'b', 'bac']


# autres tests

from random import sample
from heapq import heapify

taille = 10**5
valeurs = list(sample(range(10**9), taille))
copie = valeurs.copy()
heapify(copie)
attendu = [heappop(copie) for _ in range(taille)]
resultat = tri_par_tas(valeurs)
assert attendu == resultat
