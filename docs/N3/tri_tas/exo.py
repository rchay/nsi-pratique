#--- HDR ---#
from heapq import heappush, heappop

class TasMin:
    def __init__(self):
        self.donnees = []
    
    def est_vide(self):
        return self.donnees == []
    
    def ajoute(self, element):
        heappush(self.donnees, element)

    def extrait_min(self):
        mini = heappop(self.donnees)
        return mini
#--- HDR ---#

# La classe TasMin est incluse automatiquement pour cet exercice

def tri_par_tas(valeurs):
    ...



# tests

assert tri_par_tas([55, 42, 12, 73]) == [12, 42, 55, 73]

assert tri_par_tas(['bac', 'a', 'abc', 'b']) == ['a', 'abc', 'b', 'bac']
