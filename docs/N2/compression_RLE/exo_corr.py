def compression_RLE(texte):
    chaine_compressee = ""

    caractere_repete = texte[0]
    nb_repetitions = 0

    for caractere in texte:
        if caractere == caractere_repete:
            nb_repetitions += 1
        else:
            chaine_compressee += str(nb_repetitions) + caractere_repete
            caractere_repete = caractere
            nb_repetitions = 1

    chaine_compressee += str(nb_repetitions) + caractere_repete

    return chaine_compressee


# Tests
assert compression_RLE("aabbbbcaa") == "2a4b1c2a"
assert compression_RLE("aa aa") == "2a1 2a"
assert compression_RLE("aaa") == "3a"
assert compression_RLE("aA") == "1a1A"
