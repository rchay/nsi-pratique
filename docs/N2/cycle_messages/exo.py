def est_cyclique(plan):
    '''
    Prend en paramètre un dictionnaire plan correspondant
    à un plan d'envoi de messages entre N personnes A, B, C,
    D, E, F... (avec N <= 26).
    Renvoie True si le plan d'envoi de messages est cyclique
    et False sinon.
    '''
    personne = ...
    N = len(...)
    for i in range(...):
        personne = ...
        if personne == ...:
            return ...
    return ...

# tests

assert est_cyclique({'A': 'E', 'F': 'A', 'C': 'D', 'E': 'B', 'B': 'F', 'D': 'C'}) == False
assert est_cyclique({'A': 'E', 'F': 'C', 'C': 'D', 'E': 'B', 'B': 'F', 'D': 'A'}) == True
assert est_cyclique({'A': 'B', 'F': 'C', 'C': 'D', 'E': 'A', 'B': 'F', 'D': 'E'}) == True
assert est_cyclique({'A': 'B', 'F': 'A', 'C': 'D', 'E': 'C', 'B': 'F', 'D': 'E'}) == False

