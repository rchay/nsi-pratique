def distance(graphe, origine, arrivee):
    file = File()
    au_courant = []
    file.enfiler((origine, 0))
    while not file.est_vide():
        actuel, nb_etapes = file.defiler()
        au_courant.append(actuel)
        if actuel == arrivee:
            return nb_etapes
        for voisin in graphe[actuel]:
            if voisin not in au_courant:
                file.enfiler((voisin, nb_etapes + 1))
    return None


class File:
    def __init__(self, valeurs=None):
        if valeurs is None:
            self.valeurs = []
        else:
            self.valeurs = valeurs

    def est_vide(self):
        return len(self.valeurs) == 0

    def enfiler(self, valeur):
        self.valeurs.append(valeur)

    def defiler(self):
        return self.valeurs.pop(0)


# Tests
graphe = {'Camille':  ['Romane', 'Marion', 'Paul'],
          'Romane':   ['Nicolas', 'Antoine', 'Paul'],
          'Marion':   ['Camille', 'Romane'],
          'Paul':     ['Antoine', 'Romane'],
          'Antoine':  ['Nicolas'],
          'Nicolas':  ['Camille', 'Antoine'],
          'Stéphane': ['Antoine']}

assert distance(graphe, 'Romane', 'Romane') == 0
assert distance(graphe, 'Romane', 'Antoine') == 1
assert distance(graphe, 'Romane', 'Marion') == 3
assert distance(graphe, 'Romane', 'Stéphane') is None
