---
author: Nicolas Revéret
title: Entiers sans chiffres (POO)
tags:
    - 4-maths
    - 7-POO
---

# Écrire les entiers sans les chiffres (POO)

On souhaite dans cet exercice créer une classe `Entier` permettant de représenter des **nombres entiers positifs ou nuls**, de tester leur égalité, de les additionner et les soustraire.

On fournit la base suivante :

```python
class Entier:
    def __init__(self):
        "Crée un entier égal à zéro"
        self._liste = []

    def est_nul(self):
        "Indique si cet entier est nul ou non"
        return self._liste == []

    def incremente(self):
        "Augmente la valeur de cet entier de un"
        self._liste.append(None)

    def decremente(self):
        "Diminue, si possible, la valeur de cet entier de un"
        if self.est_nul():
            raise ValueError("L'entier est nul et n'a pas de prédécesseur")
        else:
            self._liste.pop()

    def copie(self):
        "Renvoie un entier égal à cet entier"
        resultat = Entier()
        resultat._liste = self._liste.copy()
        return resultat

    def __eq__(self, autre):
        "Teste l'égalité de deux entiers"
        return self._liste == autre._liste

    def __repr__(self):
        "Renvoie la représentation d'un objet de cet entier"
        return f"'Entier' représentant le nombre {len(self._liste)}"

    def __str__(self):
        "Renvoie la chaîne de caractère représentant cet entier"
        return f"'Entier' représentant le nombre {len(self._liste)}"
```

!!! danger "Contrainte"

    Dans tout l'exercice, il est interdit d'accéder à une valeur de, d'itérer sur, ou de modifier directement l'attribut `_liste`.
    
    On n'utilisera que les méthodes proposées dans la classe `Entier`.

La méthode `#!py __eq__` a un statut particulier. Elle fait partie des [*méthodes spéciales de Python*](https://docs.python.org/fr/3/reference/datamodel.html#special-method-names). Définir la méthode `__eq__` dans une classe permet de tester l'égalité de deux instances de cette classe en faisant `a == b`. Dans ce cas, Python exécute l'action `#!py a.__eq__(b)`.

De la même façon, `#!py __repr__` est une méthode spéciale permettant de représenter un objet dans la console.

??? note "Surcharge"

    En Python, lorsque l'on définit une nouvelle classe, les méthodes `__eq__` et `__repr__` et plusieurs autres sont créées automatiquement. Réécrire ces méthodes dans la nouvelle classe s'appelle *surcharger* une méthode.

On peut utiliser la classe `Entier` ainsi :

```pycon
>>> zero = Entier()
>>> un = Entier()
>>> un.incremente()
>>> un
'Entier' représentant le nombre 1
>>> un == zero
False
>>> autre_un = un.copie()
>>> autre_un.incremente()
>>> un == autre_un
False
>>> autre_un.decremente()
>>> un == autre_un
True
```

Les méthodes `__add__` et `__sub__` sont aussi des méthodes spéciales. Elles permettent respectivement d'additionner et de soustraire deux instances `a` et `b` de la classe `Entier` en faisant `a + b` et `a - b`. Elles sont définies ainsi :

```python
def __add__(self, autre):
    """
    Additionne cet entier à un autre entier
    Renvoie un nouvel entier
    """
    ...

def __sub__(self, autre):
    """
    Soustrait un autre entier à celui-ci
    Renvoie un nouvel entier
    """
    ...
```

Ainsi, lorsque l'on tape `a - b`, Python exécute en réalité l'instruction `a.__sub__(b)`.

On demande de compléter ces deux méthodes. Celles-ci renverront **un nouvel objet** `Entier` représentant le résultat de l'opération correspondante.

L'objet subissant la méthode et celui passé en argument **ne devront pas être modifiés**.

!!! warning "Attention"

    Certaines soustractions sont impossibles dans le cadre des entiers positifs ou nuls. Face à ce cas de figure, la méthode concernée renverra la chaîne de caractères `"Opération impossible"`.

!!! example "Exemples"

    Ci-dessous, les objets `zero`, `un`, `deux` et `trois` sont des instances de la classe `Entier`représentant les nombres entiers correspondants.

    ```pycon
    >>> resultat = zero + zero
    >>> resultat == zero
    True
    >>> resultat = un + deux
    >>> resultat == trois
    True
    >>> resultat = zero - zero
    >>> resultat == zero
    True
    >>> resultat = deux - un
    >>> resultat == un
    True
    >>> un - deux
    'Opération impossible'
    ```

{{ IDE('exo') }}
