class Entier:
    def __init__(self):
        "Crée un entier égal à zéro"
        self._liste = []

    def est_nul(self):
        "Indique si cet entier est nul ou non"
        return self._liste == []

    def incremente(self):
        "Augmente la valeur de cet entier de un"
        self._liste.append(None)

    def decremente(self):
        "Diminue, si possible, la valeur de cet entier de un"
        if self.est_nul():
            raise ValueError("L'entier est nul et n'a pas de prédécesseur")
        else:
            self._liste.pop()

    def copie(self):
        "Renvoie un entier égal à cet entier"
        resultat = Entier()
        resultat._liste = self._liste.copy()
        return resultat

    def __eq__(self, autre):
        "Teste l'égalité de deux entiers"
        return self._liste == autre._liste

    def __repr__(self):
        "Renvoie la représentation d'un objet de cet entier"
        return f"'Entier' représentant le nombre {len(self._liste)}"

    def __str__(self):
        "Renvoie la chaîne de caractère représentant cet entier"
        return f"'Entier' représentant le nombre {len(self._liste)}"

    def __add__(self, autre):
        """
        Additionne cet entier à un autre entier
        Renvoie un nouvel entier
        """
        ...

    def __sub__(self, autre):
        """
        Soustrait un autre entier à celui-ci
        Renvoie un nouvel entier
        """
        ...


# Tests
# Nombres usuels
zero = Entier()

un = Entier()
un.incremente()
un_bis = un.copie()

deux = Entier()
deux.incremente()
deux.incremente()
deux_bis = deux.copie()

trois = Entier()
trois.incremente()
trois.incremente()
trois.incremente()
trois_bis = trois.copie()

cinq = Entier()
cinq.incremente()
cinq.incremente()
cinq.incremente()
cinq.incremente()
cinq.incremente()
cinq_bis = cinq.copie()

# Additions
assert zero + zero == zero

assert un + deux == trois
assert un == un_bis and deux == deux_bis, "Les objets ne doivent pas être modifiés"

# Différences
assert zero - zero == zero

assert deux - un == un
assert un == un_bis and deux == deux_bis, "Les objets ne doivent pas être modifiés"

assert un - deux == "Opération impossible"
