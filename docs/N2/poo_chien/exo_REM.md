## Commentaires

{{ IDE('exo_corr') }}

### Remarques générales

Les trois premières méthodes (`__init__`, `age_humain` et `age_chien`) ne présentent pas de difficultés. On notera cependant l'utilisation de l'argument `self` qui permet d'indiquer que cette méthode s'applique à l'objet concerné par l'appel. Lors de l'utilisation de ces méthodes on omet ce paramètre : `toutou.age_humain()` par exemple, en effet il est passé avec `toutou`.

Les trois méthodes (`machouille`, `aboie` et `mange`) prennent deux arguments : `self` (utilisé préfixé) et un paramètre à fournir postfixé lors de l'appel.

Dans le cas de la méthode `mange`, on vérifie que `ration` satisfait les conditions à l'aide d'un test `if 0 < ration <= self.poids / 10:`. Il serait aussi possible d'utiliser un opérateur `and` : `if 0 < ration and ration <= self.poids / 10:`.

On pourrait enfin utiliser une assertion `assert 0 < ration <= self.poids / 10:`. Toutefois, cette façon de procéder génèrerait une erreur si la condition n'est pas respectée. L'utilisation du test classique permet de gérer précisément les deux cas de figure et de renvoyer `False` en cas d'échec du test.

### Attributs publics ou privés

Dans certains langages de programmation, il existe un concept d'attributs et de méthodes **privés** inaccessibles hors du corps de la classe.

Ce concept n'existe pas en Python : tous les attributs, toutes les méthodes, sont **publics** et accessibles depuis l'ensemble du programme.

Dans le cas de la classe `Chien`, il est ainsi possible de directement modifier l'attribut `age` en faisant `toutou.age = 3`.

Cela peut néanmoins autoriser des instructions incohérentes telles que `toutou.age = 100`. Pour parer à cette éventualité, il est possible de filtrer l'accès et la modification des attributs en utilisant la fonction `property`. **Cette façon de faire est une utilisation avancée de Python**, il est tout à fait possible d'utiliser la programmation orientée objet sans les `property`.

Le fonctionnement est le suivant :

```python
class Chien:
    def __init__(self, nom, age):
        self._nom = nom
        self._age = age

    def renvoie_nom(self):
        return self._nom

    def change_nom(self, nouveau):
        self._nom = nouveau

    def renvoie_age(self):
        return self._age

    def change_age(self, nouveau):
        if 0 <= nouveau <= 20:
            self._age = nouveau
        else:
            print("La valeur proposée est hors de l'intervalle [0 ; 20]")

    nom = property(renvoie_nom)
    age = property(renvoie_age, change_age)
```

L'avant-dernière ligne fait que lorsque l'utilisateur souhaite accéder à `nom` en faisant `n = toutou.nom`, Python exécute en réalité `toutou.renvoie_nom()`.

La dernière ligne ajoute quant à elles deux comportements à la classe :

* lorsque l'utilisateur souhaite accéder à l'âge grâce à l'instruction `toutou.age`, Python exécute en réalité `toutou.renvoie_age()`,
* lorsque l'utilisateur souhaite  modifier l'âge en faisant ` toutou.age = 12`, Python exécute `toutou.change_age(12)`. Cette façon de procéder permet de s'assurer que le nouvel âge est bien conforme aux conditions posées.

Dans le premier cas de figure, `nom = property(renvoie_nom)`, un seul argument est passé. Il est donc possible d'accéder au nom du chien mais **pas** de le modifier.

Dans le second, `age = property(renvoie_age, change_age)`, on passe deux arguments : le premier correspond à la méthode d'accès à la valeur, le second à celle de modification.

Les attributs stockant les valeurs du nom et de l'âge sont nommés `_nom` et `_age`. L'ajout du `_` en préfixe limite l'accès à leurs valeurs. Ainsi, lorsque l'utilisateur pense modifier l'attribut `nom`, Python modifie en réalité `_nom`.
