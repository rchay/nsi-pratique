# Tests
medor = Chien(1, 12.0)
assert medor.age_humain() == 1
assert medor.age_chien() == 20
assert medor.poids == 12.0
assert medor.machouille("bâton") == "bâto"
assert medor.aboie(3) == "OuafOuafOuaf"
assert medor.veillit() == True
assert medor.age_chien() == 28
assert medor.mange(2.0) == False
assert medor.mange(1.0) == True
assert medor.poids == 12.125
assert medor.mange(1.2125) == True

# Tests secrets
toutou = Chien(19, 10.0)
assert toutou.age == 19, "L'attribut de l'âge est mal nommé"
assert toutou.poids == 10.0, "L'attribut du poids est mal nommé"
assert toutou.age_humain() == 19, "Erreur sur la méthode age_humain"
assert toutou.age_chien() == 96, "Erreur sur la méthode age_chien"
assert toutou.poids == 10.0, "Erreur sur la méthode donne_poids"
assert toutou.machouille("balle") == "ball", "Erreur sur la méthode machouille"
assert toutou.machouille("") == "", "Erreur sur la méthode machouille"
assert toutou.aboie(30) == "Ouaf" * 30, "Erreur sur la méthode aboie"
assert toutou.aboie(0) == "", "Erreur sur la méthode aboie"
assert toutou.veillit() == True, "Erreur sur la méthide veillir"
assert toutou.veillit() == False, "Erreur sur la méthide veillir"
assert toutou.mange(-1.0) == False, "Erreur sur la méthode mange"
assert toutou.mange(11.0) == False, "Erreur sur la méthode mange"
assert toutou.mange(0.0) == False, "Erreur sur la méthode mange"
assert toutou.mange(0.5) == True, "Erreur sur la méthode mange"
assert toutou.poids == 10.0625, "Erreur sur la méthode mange"
