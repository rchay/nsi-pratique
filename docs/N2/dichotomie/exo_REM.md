# Commentaires

Il s'agit d'un algorithme classique.

{{ IDE('exo_corr') }}

A chaque itération, on se demande si la valeur centrale est égale à la `cible` :

* si oui, on renvoie immédiatement `True`
* si non, cette cible est soit :
    * supérieure à la valeur centrale : il faut chercher dans la partie droite du tableau (`debut = milieu + 1`)
    * inférieure à la valeur centrale : il faut chercher dans la partie gauche du tableau (`fin = milieu - 1`)

La zone de recherche ( (l'écart entre `fin` et `debut`) se réduit à chaque itération : si on finit par avoir `fin < debut`, c'est que 
la `cible` n'est pas dans le tableau. On sort de la boucle et renvoie `False`.

