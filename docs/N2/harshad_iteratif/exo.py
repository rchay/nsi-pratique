def somme_chiffres(n):
    ...


def harshad(n):
    assert n > 0
    ...


# Tests
assert somme_chiffres(8) == 8
assert somme_chiffres(18) == 9
assert somme_chiffres(409) == 13
assert harshad(18)
assert harshad(72)
assert harshad(11)
