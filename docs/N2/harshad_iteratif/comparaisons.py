# On compare ici différentes façons de calculer la somme des chiffres d'un entier naturel

import timeit
import random
import time


def somme_avec_str(n):
    return sum([int(k) for k in str(n)])


def somme_iter(n):
    somme = 0
    while n != 0:
        somme += n % 10
        n //= 10
    return somme


def somme_rec_non_term(n):
    if n == 0:
        return n
    else:
        return n % 10 + somme_rec_non_term(n//10)


def somme_rec_term(n, somme):
    if n == 0:
        return somme
    else:
        return somme_rec_term(n//10, somme + n % 10)


nombres = [random.randint(10**40, 2*10**40) for _ in range(100)]

# Tests des fonctions !
# print(somme_avec_str(nombres[0]))
# print(somme_iter(nombres[0]))
# print(somme_rec_non_term(nombres[0]))
# print(somme_rec_term(nombres[0],0))

# Résultats en ns
print(timeit.timeit('[somme_avec_str(n) for n in nombres]',
                    globals=globals(), number=1000, timer=time.perf_counter_ns))
print(timeit.timeit('[somme_iter(n) for n in nombres]',
                    globals=globals(), number=1000, timer=time.perf_counter_ns))
print(timeit.timeit('[somme_rec_non_term(n) for n in nombres]',
                    globals=globals(), number=1000, timer=time.perf_counter_ns))
print(timeit.timeit('[somme_rec_term(n, 0) for n in nombres]',
                    globals=globals(), number=1000, timer=time.perf_counter_ns))
