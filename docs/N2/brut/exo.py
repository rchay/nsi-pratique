def extrait_texte_brut(source_html):
    # texte_brut contiendra le texte_brut brut extrait du fichier, i est la position de parcourt du fichier html
    texte_brut = ""
    i = 0
    while i<len(source_html):
        # Si on rencontre un "<" (début de balise), on parcourt le fichier sans le recopier jusqu'à ">" (fin de balise)
        if source_html[i]=="<":
            while source_html[i]!=">":
                i += 1
        # Sinon on recopie le fichier dans texte_brut
        else:
            texte_brut += source_html[i]
        i += 1
    return texte_brut


