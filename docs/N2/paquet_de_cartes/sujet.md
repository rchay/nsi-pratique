---
author: BNS2022-5.2, puis Franck Chambon
title: Paquet de cartes BUG!!!! TODO
tags:
  - POO
---

!!! bug "Exercice **très** problématique"
    Tentative pour le sauver... à voir...

    Problème 1 : il faut donner une façon explicite de remplir le paquet

    Problème 2 : `valeur` est mal nommé. Ce n'est pas un nombre. "Dame" est une valeur ; pas 12 !!!

# Paquet de cartes BUG!!!! TODO

On dispose d'un programme permettant de créer un objet de type `PaquetDeCarte`, selon les éléments indiqués dans le code ci-dessous.

- Compléter ce code aux endroits indiqués par `...`
    - `#! pass` est une possibilité ; l'instruction qui ne fait rien.
- Pour ajouter des gestions d'erreurs dans l'initialisation de `Carte`, ainsi que dans la méthode `donne_carte()`, on pourra utiliser des assertions avec une instruction comme `#!py assert 1 <= couleur <= 4, "Mauvaise couleur"`. Les messages transmis seront :
    - `"Mauvaise couleur"`
    - `"Mauvaise valeur"`
    - `"Paquet vide"`

```python
class Carte:
    def __init__(self, couleur, valeur):
        "Initialise couleur (de 1 à 4), et valeur (de 1 à 13)"
        ...  # tests et gestion d'erreur
        self.couleur = couleur
        self.valeur = valeur

    def donne_nom(self):
        """Renvoie le nom de la Carte :
           'As', '2', --- '10', 'Valet', 'Dame', 'Roi'
        """

        if 1 < self.valeur < 11:
            return str(self.valeur)
        elif self.valeur == 11:
            return "Valet"
        elif self.valeur == 12:
            return ...
        elif self.valeur == 13:
            return "Roi"
        elif self.valeur == 1:
            return "As"
        else:
            ...  # commentaire à modifier aussi

    def donne_couleur(self):
        """Renvoie la couleur de la Carte :
            'pique', 'coeur', 'carreau', 'trefle'
        """
        ENSEIGNES = ['', 'pique', 'coeur', 'carreau', 'trefle']
        return ENSEIGNES[self.Couleur]

class PaquetDeCarte:
    def __init__(self):
        self.contenu = []
    
    def remplir(self):
        "Remplit le paquet de cartes"
        ...

    def donne_carte(self, position):
        "Renvoie la `Carte` qui se trouve à la position donnée"
        ...
```

!!! exemple "Exemple au hasard"

    ```pycon
    >>> un_paquet = PaquetDeCarte()
    >>> un_paquet.remplir()
    >>> carte = un_paquet.donne_carte(20)
    >>> print(carte.donne_nom() + " de " + carte.donne_couleur())
    6 de coeur
    ```

{{ IDE() }}
