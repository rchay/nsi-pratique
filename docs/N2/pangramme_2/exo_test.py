# Tests
assert indice("a") == 0
assert indice("z") == 25
assert minuscule(1) == "b"
assert minuscule(24) == "y"
assert lettres_manquantes("portez ce vieux whisky au juge blond qui fume !") == []
assert lettres_manquantes("portez un vieux whisky au juge blond qui fume !") == ["c"]
assert lettres_manquantes("portez ce vieux whisky au juge blond !") == ["f", "m", "q"]

# Tests supplémentaires
import string

alphabet = list(string.ascii_lowercase)
for i, lettre in enumerate(alphabet):
    assert indice(lettre) == i
    assert minuscule(i) == lettre
assert lettres_manquantes("a") == alphabet[1:]
assert lettres_manquantes("") == alphabet
assert lettres_manquantes(" ".join(alphabet[2:])) == ["a", "b"]
assert lettres_manquantes(", ".join(alphabet)) == []
