def indice(minuscule):
    return ord(minuscule) - ord("a")


def minuscule(i):
    return chr(ord("a") + i)


def lettres_manquantes(phrase):
    presences = [False] * 26
    for caractere in phrase:
        if "a" <= caractere <= "z":
            presences[indice(caractere)] = True

    resultat = []
    for i in range(26):
        if not presences[i]:
            resultat.append(minuscule(i))

    return resultat


# Tests
assert indice("a") == 0
assert indice("z") == 25
assert minuscule(1) == "b"
assert minuscule(24) == "y"
assert lettres_manquantes("portez ce vieux whisky au juge blond qui fume !") == []
assert lettres_manquantes("portez un vieux whisky au juge blond qui fume !") == ["c"]
assert lettres_manquantes("portez ce vieux whisky au juge blond !") == ["f", "m", "q"]
