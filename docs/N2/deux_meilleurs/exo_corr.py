def deux_meilleurs(scores):
    max1, max2 = None, None
    for v in scores:
        if max1 is None or v > max1:
            max1, max2 = v, max1
        elif max2 is None or v > max2:
            max2 = v
    return max1, max2
