# Commentaires

## Autre version

Dans le premier test, on peut aussi faire une inégalité large :
```python
def deux_meilleurs(scores):
    max1, max2 = None, None
    for v in scores:
        if max1 is None or v >= max1:
            max1, max2 = v, max1
        elif max2 is None or v > max2:
            max2 = v
    return max1, max2
```

## Version sans tester `None`

On peut faire une façon plus proche d'une recherche de maximum classique
:
```python
def deuxieme_max(scores):
    if len(scores) == 0:
        return None, None
    elif len(scores) == 1:
        return scores[0], None
    if scores[0] > scores[1]:
        max1, max2 = scores[0], scores[1]
    else:
        max1, max2 = scores[1], scores[0]
    for i in range(2, len(scores)):
        if scores[i] > max1:
            max2 = max1
            max1 = scores[i]
        elif scores[i] > max2:
            max2 = scores[i]
    return max1, max2
```

## Valeurs distinctes

Si on veut avoir deux valeurs strictement différentes, il faut faire un
peu plus attention lors du deuxième test dans la boucle. Mais ce sera
l'objet d'un autre exercice.
