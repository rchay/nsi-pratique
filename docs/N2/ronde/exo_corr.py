def indice_prochain_True(booleens, indice):
    taille = len(booleens)
    while True:
        indice += 1
        if indice == taille:
            indice = 0
        if booleens[indice]:
            return indice


def gagnant(nb_joueurs):
    joueurs = [True for _ in range(nb_joueurs)]
    indice_joueur = 0
    while nb_joueurs > 1:
        prochain = indice_prochain_True(joueurs, indice_joueur)
        joueurs[prochain] = False
        indice_joueur = indice_prochain_True(joueurs, prochain)
        nb_joueurs -= 1

    return indice_joueur+1


# Tests
# Tests de la fonction indice_prochain_True
booleens = [True, True, False, True, False]
assert indice_prochain_True(booleens, 0) == 1
assert indice_prochain_True(booleens, 1) == 3
assert indice_prochain_True(booleens, 3) == 0

# Tests de la fonction gagnant
assert gagnant(1) == 1
assert gagnant(5) == 3
assert gagnant(13) == 11
