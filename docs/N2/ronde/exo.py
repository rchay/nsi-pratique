def indice_prochain_True(booleens, indice):
    taille = len(booleens)

    while True:
        indice += ...
        if indice == taille:
            ...
        if booleens[...]:
            return ...


def gagnant(nb_joueurs):
    joueurs = [True for _ in range(nb_joueurs)]
    indice_joueur = ...
    while ...:
        prochain = ...
        joueurs[prochain] = False
        indice_joueur = ...
        nb_joueurs = ...

    return ...


# Tests
# Tests de la fonction indice_prochain_True
booleens = [True, True, False, True, False]
assert indice_prochain_True(booleens, 0) == 1
assert indice_prochain_True(booleens, 1) == 3
assert indice_prochain_True(booleens, 3) == 0

# Tests de la fonction gagnant
assert gagnant(1) == 1
assert gagnant(5) == 3
assert gagnant(13) == 11
