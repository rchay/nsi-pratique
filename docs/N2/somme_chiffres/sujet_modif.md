---
author: Benoît LE GOUIS
title: Somme des chiffres d'un nombre
tags:
    - boucle
    - maths
---

# Somme des chiffres pour un nombre en écriture décimale

On veut calculer la somme des chiffres d'un nombre en écriture décimale. Ce genre d'opération est effectué de manière naturelle en primaire pour déterminer si un nombre
est un multiple de 3 ou de 9 : on fait la somme de ses chiffres, et on regarde si cette somme est elle-même un multiple de 3 ou de 9. 

En revanche, là où il est très facile pour un humain de savoir quels chiffres composent un nombre, cette décomposition ne va pas de soi pour un ordinateur.
Cet exercice réalise cette opération en deux étapes :

1. une première étape où on décompose un nombre : on compte le nombre d'occurrences de chacun de ses chiffres et on stocke ce résultat dans un tableau d'effectifs ; c'est-à-dire un tableau où chaque indice de $0$ à $9$ représente un chiffre et la valeur associée est le nombre d'occurrences de ce chiffre dans le nombre 
2. une deuxième étape où on prend un tel tableau et on calcule la somme des chiffres associée.

L'étape 1 est réalisée par la fonction `decomposition` et l'étape 2 par la fonction `somme_chiffres`.


!!! example "Exemple"
    Ainsi le nombre $222270$ a pour tableaux d'effectifs `[1, 0, 4, 0, 0, 0, 0, 1, 0, 0]` et pour somme des chiffres 
    
    $$1×0 + 0×1 + 4×2 + 0×3+ 0×4 + 0×5 + 0×6 + 1×7+ 0×8 + 0×9 = 8+7 = 15$$

    ```pycon
    >>> decomposition(222270)
    [1, 0, 4, 0, 0, 0, 0, 1, 0, 0]
    >>> somme_chiffres([1, 0, 4, 0, 0, 0, 0, 1, 0, 0])
    15
    ```

## Question 1

Compléter la fonction `somme_chiffres` qui prend en argument un tableau d'occurrences de chiffres `effectif_chiffres`, comme décrit en introduction et qui renvoie la somme des chiffres ainsi comptés.

!!! example "Exemples"

    ```pycon
    >>> somme_chiffres([1, 1, 1, 1, 1, 1, 1, 1, 1, 1])
    45
    >>> somme_chiffres([0, 0, 0, 0, 0, 0, 0, 0, 0, 1])
    9
    >>> somme_chiffres([0, 9, 0, 0, 0, 0, 0, 0, 0, 0])
    9
    ```


## Question 2

Compléter la fonction `decomposition` qui prend en argument un nombre entier positif `nombre` et qui renvoie le tableau `effectifs` tel que décrit en introduction.

!!! tip "Rappels"
    
    - L'opérateur `//` renvoie le résultat de la division euclidienne (par exemple, `13//4` renvoie la valeur `3`).
    
    - L'opérateur `%` en python renvoie le reste de la division euclidienne (par exemple, `13%4` renvoie la valeur `1`). 


!!! example "Exemples"

    ```pycon
    >>> decomposition(342424)
    [0, 0, 2, 1, 3, 0, 0, 0, 0, 0]
    >>> decomposition(10111213141516171819)
    [1, 11, 1, 1, 1, 1, 1, 1, 1, 1]
    >>> decomposition(1234567890)
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
    >>> decomposition(1337 ** 42)
    [18, 13, 15, 8, 14, 9, 12, 8, 16, 19]
    ```

{{ IDE('exo') }}
