Autre version légèrement plus synthétique :


```python
def somme_chiffres(effectifs_chiffres):
    resultat = 0
    for chiffre, effectif in enumerate(effectifs_chiffres):
        resultat += chiffre * effectif
    return resultat 

def decomposition(nombre):
    effectifs_chiffres = [0] * 10
    while nombre != 0:
        nombre, unite = divmod(nombre, 10)
        effectifs_chiffres[unite] += 1
    return effectifs_chiffres
```