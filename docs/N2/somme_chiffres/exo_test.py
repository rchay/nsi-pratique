def somme_chiffres(effectifs_chiffres):
    assert len(effectifs_chiffres) == 10, Le tableau doit être de taille 10
    resultat = 0
    for i in range(10):
        resultat = resultat + effectifs_chiffres[i] * i
    return resultat

def decomposition(nombre):
    effectifs_chiffres = [0] * 10
    while nombre != 0:
        nombre, unite = nombre // 10, nombre % 10
        effectifs_chiffres[unite] = effectifs_chiffres[unite] + 1
    return effectifs_chiffres



#Exercice 1
assert somme_chiffres([0, 0, 0, 0, 0, 0, 0, 0, 0, 0]) == 0
assert somme_chiffres([1, 1, 1, 1, 1, 1, 1, 1, 1, 1]) == 45
assert somme_chiffres([0, 0, 0, 0, 0, 0, 0, 0, 0, 1]) == 9
assert somme_chiffres([0, 9, 0, 0, 0, 0, 0, 0, 0, 0]) == 9
assert somme_chiffres(decomposition(111223)) == 10
assert somme_chiffres(decomposition(1337 ** 42)) == 595

#Exercice 2
assert decomposition(0) == [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
assert decomposition(1234567890) == [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
assert decomposition(111223) == [0, 3, 2, 1, 0, 0, 0, 0, 0, 0]
assert decomposition(312121) == [0, 3, 2, 1, 0, 0, 0, 0, 0, 0]
assert decomposition(10111213141516171819) == [1, 11, 1, 1, 1, 1, 1, 1, 1, 1]
assert decomposition(1337 ** 42) == [18, 13, 15, 8, 14, 9, 12, 8, 16, 19]

