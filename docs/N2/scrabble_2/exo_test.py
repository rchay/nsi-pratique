# tests
assert calcul_score("GIRAFE","-+---#") == 22
assert calcul_score("KAYAK", "--+--") == 42
assert calcul_score("INFORMATIQUE", "--@---------") == 69



# autres tests
assert calcul_score("OXYGENER", "@--+---@") == 261
assert calcul_score("PROGRAMME", "*---*---*") == 24
assert calcul_score("PYTHON", "------") == 20


