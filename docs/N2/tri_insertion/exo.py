def tri_insertion(tableau):
    n = len(tableau)
    for i in range(1, n):
        j = ...
        valeur_a_inserer = tableau[...]
        while j > ... and valeur_a_inserer < tableau[...]:
            tableau[j] = tableau[j-1]
            j = ...
        tableau[j] = ...


# Tests
tableau_0 = [9, 5, 8, 7, 6]
tri_insertion(tableau_0)
assert tableau_0 == [5, 6, 7, 8, 9]

tableau_1 = [2, 5, -1, 7, 0, 28]
tri_insertion(tableau_1)
assert tableau_1 == [-1, 0, 2, 5, 7, 28]

un_seul = [9]
tri_insertion(un_seul)
assert un_seul == [9]

tableau_vide = []
tri_insertion(tableau_vide)
assert tableau_vide == []
