VALEUR = {"I": 1, "V": 5, "X": 10, "L": 50, "C": 100, "D": 500, "M": 1000}

def valeur_romains(chaine):
    n = len(chaine)
    cumul = 0
    for i in range(n - 1):
        if VALEUR[chaine[i]] < VALEUR[chaine[i + 1]]:
            cumul = cumul - VALEUR[chaine[i]]
        else:
            cumul = cumul + VALEUR[chaine[i]]
    cumul = cumul + VALEUR[chaine[n - 1]]
    return cumul

assert valeur_romains("XVI") == 16
assert valeur_romains("MMXXII") == 2022
assert valeur_romains("CDII") == 402
assert valeur_romains("XLII") == 42

