VALEUR = {"I": 1, "V": 5, "X": 10, "L": 50, "C": 100, "D": 500, "M": 1000}

def valeur_romains(chaine):
    n = len(chaine)
    cumul = 0
    for i in range(n - 1):
        if ...
            cumul = cumul - VALEUR[chaine[i]]
        else:
            ...
    cumul = ...
    return cumul

assert valeur_romains("XVI") == 16
assert valeur_romains("MMXXII") == 2022
assert valeur_romains("CDII") == 402
assert valeur_romains("XLII") == 42

