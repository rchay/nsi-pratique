def distance_levenshtein(chaine1: str, chaine2: str) -> int:
    pass


# Test de deux chaines identiques
assert distance_levenshtein("distance", "distance") == 0
# Test de chaines avec une substitution
assert distance_levenshtein("distance", "distante") == 1
# Test de chaines avec un ajout
assert distance_levenshtein("distance", "distances") == 1
# Test de chaines avec 3 suppressions
assert distance_levenshtein("distance", "pense") == 6
# Test de chaines avec 3 substitutions
assert distance_levenshtein("distance", "dispense") == 3
# Test de chaines différentes
assert distance_levenshtein("distance", "haricots") == 8
