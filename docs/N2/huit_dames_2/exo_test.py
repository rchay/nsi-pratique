
# Tests
# colonne  0  1  2  3  4  5  6  7
valide = [6, 4, 2, 0, 5, 7, 1, 3]

# colonne   0  1  2
invalide = [1, 2, 0]
invalide_2 = [2, 4, 1, 5, 3, 6, 0, 7]

assert disposition_valide(valide)
assert not disposition_valide(invalide)
assert not disposition_valide(invalide_2)

# Tests supplémentaires
valide_1 = [2, 0, 6, 4, 7, 1, 3, 5]
valide_2 = [4, 0, 3, 5, 7, 1, 6, 2]
valide_3 = [3, 0, 2, 4, 1]
invalide_1 = [2, 0, 6, 4, 7, 2, 3, 5]
invalide_2 = [4, 0, 3, 7, 5, 2, 6, 2]
invalide_3 = [4, 0, 3, 7, 5, 2, 6, 2]
invalide_4 = [3, 2, 0, 4, 1]
assert disposition_valide(valide_1)
assert disposition_valide(valide_2)
assert disposition_valide(valide_3)
assert not disposition_valide(invalide_1)
assert not disposition_valide(invalide_2)
assert not disposition_valide(invalide_3)
assert not disposition_valide(invalide_4)
