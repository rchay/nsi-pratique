def disposition_valide(disposition):
    n = len(disposition)

    # Vérifications des lignes
    lignes_occupees = [...] * ...
    for ligne in disposition:
        if ...[...]:
            return False
        else:
            ...[...] = ...
    
    # Vérification des diagonales
    for j1 in range(n - 1):
        for j2 in range(j + 1, n):
            if abs(... - ...) == (... - ...):
                return False
    return True


# Tests
# colonne  0  1  2  3  4  5  6  7
valide =  [6, 4, 2, 0, 5, 7, 1, 3]

# colonne   0  1  2
invalide = [1, 2, 0]
invalide_2 = [2, 4, 1, 5, 3, 6, 0, 7]

assert disposition_valide(valide)
assert not disposition_valide(invalide)
assert not disposition_valide(invalide_2)
