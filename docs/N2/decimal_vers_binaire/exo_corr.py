BITS = ['0', '1']

def binaire(nombre):
    nombre_binaire = BITS[nombre%2]
    nombre = nombre // 2
    while nombre > 0:
        nombre_binaire = BITS[nombre%2] + nombre_binaire
        nombre = nombre // 2
    return nombre_binaire

# tests

assert binaire(0) == '0'
assert binaire(1) == '1'
assert binaire(16) == '10000'
assert binaire(77) == '1001101'
