def ajoute_espaces(mot, longueur):
    return ... + " " * (...)


def barres(categories, valeurs):
    longueur_max = ...
    for mot in ...:
        if len(mot) > ...:
            longueur_max = ...

    diagramme = ""
    for i in range(len(categories)):
        diagramme += ...  # la catégorie
        diagramme += " : "
        diagramme += ...  # la barre
        diagramme += "\n"  # retour à la ligne

    return ...


# Tests
# Fonction ajoute_espaces
assert ajoute_espaces("Anne", 7) == "Anne   "
assert ajoute_espaces("Luc", 7) == "Luc    "
assert ajoute_espaces("nsi", 3) == "nsi"
assert ajoute_espaces("", 2) == "  "
# Fonction barres
categories = ["Anne", "Luc", "Patrick", "Sam"]
valeurs = [10, 8, 5, 15]
diagramme = """Anne    : ##########
Luc     : ########
Patrick : #####
Sam     : ###############
"""
assert barres(categories, valeurs) == diagramme
categories = ["A", "B", "", "EEEEE"]
valeurs = [1, 1, 0, 5]
diagramme = """A     : #
B     : #
      : 
EEEEE : #####
"""
assert barres(categories, valeurs) == diagramme
