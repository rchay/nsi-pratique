# Commentaires

Le code est construit autour de deux fonctions, `ajoute_espaces` et `barre`.

{{ IDE('exo_corr') }}

Dans la fonction `ajoute_espaces`, on ajoute des caractères à la chaîne passée en paramètre. Celle-ci étant de longueur inférieure ou égale à la `longueur` souhaitée, on peut faire `return mot + " " * (longueur-len(mot))`, la différence valant au minimum `0` (ce qui n'ajoute aucun espace).

La fonction `barres` nécessite deux étapes :

* dans un premier temps, on recherche la longueur maximale des chaînes dans la liste `categories`,
* une fois cette longueur trouvée, on crée le `diagramme` (une chaîne vide) et pour chaque indice `i` entre `0` et `len(categories)` (exclus) :
    * on ajoute la catégorie avec suffisamment d'espaces,
    * on ajoute les `: `,
    * on dessine la barre,
    * on ajoute un retour à la ligne `\n`.

La garantie que les deux listes `categories` et `valeurs` ont la même longueur nous assure que la boucle s'exécute sans erreur.