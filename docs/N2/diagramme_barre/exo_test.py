
# Tests
assert ajoute_espaces("Anne", 7) == "Anne   "
assert ajoute_espaces("Luc", 7) == "Luc    "
assert ajoute_espaces("nsi", 3) == "nsi"
assert ajoute_espaces("", 2) == "  "
categories = ["Anne", "Luc", "Patrick", "Sam"]
valeurs = [10, 8, 5, 15]
diagramme = """Anne    : ##########
Luc     : ########
Patrick : #####
Sam     : ###############
"""
assert barres(categories, valeurs) == diagramme
categories = ["A", "B", "", "EEEEE"]
valeurs = [1, 1, 0, 5]
diagramme = """A     : #
B     : #
      : 
EEEEE : #####
"""
assert barres(categories, valeurs) == diagramme

# Tests supplémentaires
for longueur in range(0, 20):
    assert ajoute_espaces("abc", 3 + longueur) == "abc"+" " * \
        longueur, f"Erreur pour une longueur finale de taille {longueur}"
    assert ajoute_espaces("", longueur) == " " * \
        longueur, f"Erreur pour une longueur finale de taille {longueur}"

for taille in range(1, 10):
    categories = ["a"*k for k in range(1, taille+1)]
    valeurs = list(range(1, taille+1))
    diagramme = "\n".join([ajoute_espaces(
        categories[i], taille)+" : "+"#"*valeurs[i] for i in range(taille)])+"\n"
    assert barres(categories, valeurs) == diagramme
