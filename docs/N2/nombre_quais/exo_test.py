# Tests
trains = [(3, 5), (2, 4), (6, 8)]
assert nb_min_quais(trains) == 2

trains = [(1, 5), (6, 7), (5, 5.99)]
assert nb_min_quais(trains) == 1

trains = [(1, 2), (2, 3), (3, 4), (4, 5)]
assert nb_min_quais(trains) == 1

trains = []
assert nb_min_quais(trains) == 0


# Tests secrets
trains = [(k, k+1) for k in range(50)]
assert nb_min_quais(trains) == 1

trains = [(k, k+1) for k in range(20, 0, -1)]
assert nb_min_quais(trains) == 1

trains = [(k, k+2) for k in range(50)]
assert nb_min_quais(trains) == 2

trains = [(0, 20-k) for k in range(10)]
assert nb_min_quais(trains) == 10
