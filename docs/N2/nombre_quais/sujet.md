---
author: Nicolas Revéret
title: Nombre de quais
tags: 
    - liste
    - tuple
state : relecture
---

# Nombre minimal de quais

Le chef de gare a devant lui la liste des trains passant par sa gare dans la journée.

Cette liste est donnée sous forme d'une liste de couples de nombres : pour chaque train on fournit son heure d'arrivée en gare et son heure de départ.

!!! example "Exemple"

    ```pycon
    trains = [(3, 5), (2, 4), (6, 8)]
    ```

    Trois trains vont passer en gare dans la journée :

    * le premier arrivera à 3h et repartira à 5h,
  
    * le deuxième arrivera à 2h et repartira à 4h,
  
    * le dernier arrivera à 6h et repartira à 8h.

On garantit : 

* que l'heure de départ de chaque train est strictement supérieure à son heure d'arrivée,
  
* que lorsqu'un train arrive en gare ou la quitte, cet évènement est instantané. Il n'y a pas de temps de freinage ou d'accélération...

Lorsqu'un train est en gare il est stationné le long d'un quai et n'en bouge pas. Chaque quai ne peut accueillir qu'un seul train à la fois.

Le chef de gare se demande **combien de quai au minimum il va devoir utiliser dans la journée afin de pouvoir accueillir tous les trains ?**

!!! example "Exemples"

    En reprenant la liste `[(3, 5), (2, 4), (6, 8)]`, il est possible de n'utiliser que 2 quais.

    Dans le cas `[(1, 5), (6, 7), (5, 5.99)]`, il est possible de n'utiliser qu'un seul quai.

La première étape de la résolution consiste à construire la liste des évènements associés aux trains de la journée. On appelle "*évènement*" un couple de nombres contenant :

* l'heure d'arrivée ou de départ d'un train,
* la variation du nombre de trains présents en gare : `1` si l'heure correspond à l'arrivée d'un train, `-1` si c'est un départ.

!!! example "Exemple"

    La liste des évènements associée à `[(3, 5), (2, 4), (6, 8)]` est `[(3, 1), (5, -1), (2, 1), (4, -1), (6, 1), (8, -1)]`.

    Dans cette liste :
    
    * le couple `(3, 1)` indique qu'à 3h, un train arrive en gare (il y a un train de **plus** stationné),
    
    * le couple `(4, -1)` indique qu'à 4h, un train quitte la gare (il y a un train de **moins** stationné).

Une fois cette liste des évènements créée, on pourra la trier et l'utiliser afin de calculer le nombre minimal de quais nécessaires.


!!! example "Exemples"

    ```pycon
    >>> trains = [(3, 5), (2, 4), (6, 8)]
    >>> nb_min_quais(trains)
    2
    >>> trains = [(1, 5), (6, 7), (5, 5.99)]
    >>> nb_min_quais(trains)
    1
    >>> trains = [(1, 2), (2, 3), (3, 4), (4, 5)]
    >>> nb_min_quais(trains)
    1
    >>> trains = []
    >>> nb_min_quais(trains)
    0
    ```

{{ IDE('exo') }}
