def digicrack(digicode):
    ...


# tests

def digicode1(a, b, c, d):
    return not bool ((((a ^ b ^ 6) << ((c & 7) + d)) + ((a + c) ** b + d * 17 + 2022)) % 9973)
assert digicrack(digicode1) == (2, 8, 7, 4), "erreur digicode1"

def digicode2(a, b, c, d):
    return not bool ((((a ^ b ^ 6) << ((c & 7) + d)) + ((a + c) ** b + d * 17 + 1749)) % 9973)
assert digicrack(digicode2) == (2, 9, 8, 6), "erreur digicode2"
