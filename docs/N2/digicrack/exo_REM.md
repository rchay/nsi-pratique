# Commentaires

Bien qu'utiliser 4 boucles for imbriquées (une pour chaque chiffres `a`, `b`, `c` et `d`) soit possible, il est préférable d'utiliser une unique boucle for qui soit à même de générer tous les tuples `(a, b, c, d)`.

Les tuples forment toutes les écritures possibles 4 chiffres de `0000` à `9999`. On peut dès lors utiliser une boucle for pour parcourir tous les nombres de 0 à 9999 et générer pour chacun la combinaison `(a, b, c, d)` en se servant des opérations de division entière `//` (par 10, 100 et 1000) et de modulo `% 10` pour reconstituer les 4 chiffres.

