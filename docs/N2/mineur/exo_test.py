from random import randint

# Tests
mine = [[1, 2, 3],
        [0, 1, 0],
        [0, 1, 0],
        [4, 0, 0]]
assert meilleure_richesse_cumulee(mine) == 9


mine = [[1, 2, 5]]
assert meilleure_richesse_cumulee(mine) == 5


mine = [[7, 9, 10, 7, 10, 8,  5],
        [5, 3,  1, 7,  5, 7,  1],
        [8, 9,  7, 7,  2, 3,  2],
        [7, 7,  2, 4,  6, 5,  7],
        [6, 7,  1, 6,  6, 6,  1],
        [3, 2,  5, 3,  2, 2, 10]]
assert meilleure_richesse_cumulee(mine) == 46


# Tests secrets
def mine_aleatoire(largeur, hauteur, mini=1, maxi=10):
    return [[randint(mini, maxi) for k in range(largeur)] for _ in range(hauteur)]


def meilleure_richesse_maximale_corr(mine):
    hauteur, largeur = dimensions(mine)
    richesses_cumulees = [0]*largeur
    for i in range(hauteur-1, -1, -1):
        temp = [0]*largeur
        temp[0] = mine[i][0] + \
            max(richesses_cumulees[0], richesses_cumulees[1])
        for j in range(1, largeur-1):
            temp[j] = mine[i][j] + max(richesses_cumulees[j-1],
                                       richesses_cumulees[j],
                                       richesses_cumulees[j+1])
        temp[largeur-1] = mine[i][largeur-1] + \
            max(richesses_cumulees[largeur-2], richesses_cumulees[largeur-1])
        richesses_cumulees = temp
    return max(richesses_cumulees)


mine = mine_aleatoire(15, 15)
for _ in range(10):
    mine = mine_aleatoire(15, 15)
    maxi_calcul = meilleure_richesse_cumulee(mine)
    maxi_attendu = meilleure_richesse_maximale_corr(mine)
    assert maxi_calcul == maxi_attendu, "Erreur sur une mine différente"
