INT_SIZE: int = 8


def additionneur(a: int, b: int, c: int) -> tuple[int, int]:
    return a ^ b ^ c, (a & b) | (a & c) | (b & c)


def addition_binaire(n1: list[int], n2: list[int]) -> list[int]:
    retenue: int = 0
    resultat: list[int] = []
    for i in range(INT_SIZE):
        bit, retenue = additionneur(n1[len(n1) - 1 - i], n2[len(n1) - 1 - i], retenue)
        resultat = [bit] + resultat
    return resultat


assert addition_binaire([0, 0, 0, 0, 1, 0, 1, 0], [0, 0, 0, 0, 0, 1, 0, 1]) == [
    0,
    0,
    0,
    0,
    1,
    1,
    1,
    1,
], "cas simple des entiers positifs"
assert addition_binaire([0, 1, 1, 1, 1, 1, 1, 1], [0, 0, 0, 0, 0, 0, 0, 1]) == [
    1,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
], "le nombre suivant 2^7 - 1"
assert addition_binaire([1, 1, 1, 1, 1, 1, 1, 1], [0, 0, 0, 0, 0, 0, 0, 1]) == [
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
], "le nombre suivant -1"

def complement_a_1(bit):
    return 1 - bit

def complement_a_2(octet):
    for indice in range(8):
        octet[indice] = complement_a_1(octet[indice])
    return addition_binaire(octet, [0, 0, 0, 0, 0, 0, 0, 1])


assert complement_a_2([0, 0, 0, 0, 1, 0, 1, 0]) == [1, 1, 1, 1, 0, 1, 1, 0]
