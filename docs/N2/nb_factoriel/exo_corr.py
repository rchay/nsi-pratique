factorielle_mem = [1]

def factorielle(n):
    i = len(factorielle_mem)
    while n >= i:
        factorielle_mem.append(factorielle_mem[i - 1] * i)
        i += 1
    return factorielle_mem[n]

# tests

assert factorielle(5) == 120
assert factorielle(10) == 3628800
