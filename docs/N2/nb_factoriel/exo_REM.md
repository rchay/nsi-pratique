## Commentaires

### Version directe

{{ IDE('exo_corr') }}

### Version détaillée

Si la version ci-dessus n'est pas explicite pour vous, une version détaillée est disponible.

```py
factorielle_mem = [1]

def factorielle(n):
    i = len(factorielle_mem)
    while n >= i:
        fact_im1 = factorielle_mem[i - 1]
        fact_i = fact_im1 * i
        factorielle_mem.append(fact_i)
        i += 1
    return factorielle_mem[n]
```
