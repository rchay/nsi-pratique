# tests

file = File()
assert file.est_vide()
file.enfile(2)
assert not file.est_vide()
file.enfile(5)
file.enfile(7)
assert file.defile() == 2
assert file.defile() == 5

# autres tests

file = File()
file.enfile(1)
file.enfile(2)
assert file.defile() == 1
file.enfile(3)
file.enfile(4)
assert file.defile() == 2
assert file.defile() == 3
file.enfile(5)
assert file.defile() == 4
assert file.defile() == 5
assert file.est_vide()
file.enfile(6)
file.enfile(7)
assert not file.est_vide()
assert file.defile() == 6
assert file.defile() == 7
assert file.est_vide()
