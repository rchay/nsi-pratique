# Commentaires

## Représenter une file avec une liste chaînée

Il est aussi possible d'utiliser une liste chaînée classique, en n'ayant
une référence qu'au permier maillon, mais dans ce cas, il faut parcourir
toute la liste pour les ajouts ou les retraits, selon le sens
d'insertion.

## `return None`

Dans la méthode `defile`, le `else: return None` est inutile puisque
s'il un appel de fonction ne se termine pas par un `return`, la valeur
`None` est automatiquement renvoyée.
