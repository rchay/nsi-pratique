---
author: Franck Chambon
title: Hofstadter Figure-Figure
tags:
  - 6-récursivité
  - 9-prog.dynamique
---
# Suite de Hofstadter Figure-Figure

Les suites *Hofstadter Figure-Figure* $R$ et $S$ sont des suites d'entiers non nuls, complémentaires, définies par :

- $R_1 = 1$, $S_1 = 2$.
- $R_n = R_{n - 1} + S_{n-1}$, pour $n > 1$.
- avec la suite $(S_{n})$ définie comme strictement croissante contenant tous les entiers absents de $(R_{n})$.

Les premiers termes sont :

- $R = (1, 3, 7, 12, 18, 26, 35, 45, 56, 69, 83, 98, 114, 131, 150, 170, 191, 213, 236, 260, \cdots)$
- $S = (2, 4, 5, 6, 8, 9, 10, 11, 13, 14, 15, 16, 17, 19, 20, 21, 22, 23, 24, 25, \cdots)$

Construire deux tableaux de taille au moins 10000 chacun pour stocker $R$, et $S$. On les débutera avec `#!py None` pour $R_0$ et $S_0$ qui ne sont pas définis.

!!! example "Exemples"
    ```pycon
    >>> R[3]
    7
    >>> S[3]
    5
    >>> (len(R) >= 10000) and (len(S) >= 10000)
    True
    ```

!!! warning Contraintes
    On n'utilisera pas ni les ensembles, ni les dictionnaires.
    
    On n'utilisera que les deux tableaux à construire comme structure de données.

    On **n'utilisera pas** de boucle pour déterminer l'appartenance à $R$, ni le test `#!py x in R` (qui revient à faire une boucle).

    On construira plutôt `R` et `S` à des vitesses distinctes, chacune avec leur indice. Il n'est pas interdit de modifier un peu l'initialisation de `R` et `S`.


{{ IDE('exo') }}

!!! tip "Indice 0"
    Ne pas hésiter à regarder l'indice 1 si vous restez bloqué trop longtemps. L'indice 1 ne donne qu'une petite piste, mais qui peut bien débloquer.

??? tip "Indice 1"
    On pourra commencer avec les valeurs

    ```python
    R = [None, 1, 3]
    S = [None, 2, 4, 5, 6]
    ```
    On cherchera à ajouter une valeur à $R$, et plusieurs à $S$.

??? tip "Indice 2"
    On pourra ensuite suivre l'idée

    ```python
    i_r = 2  # l'indice du dernier élément de R
    r_suivant = 7
    while i_r < 10000:
        R.append(r_suivant)
        i_r += 1
        prochain = ...
        ...
        r_suivant = prochain
    ```
