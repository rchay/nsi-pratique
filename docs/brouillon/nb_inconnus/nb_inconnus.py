C = [1, 1]
from math import factorial as f
def binom(n, k): return f(n) // f(k) // f(n-k)

for n in range(1, 10):
    C.append(sum(binom(n, k) * C[k] * C[n - k]   for k in range(n+1)))

print(C)
