assert catalan_recursif(0) == 1
assert catalan_recursif(1) == 1
assert catalan_recursif(2) == 2
assert catalan_recursif(3) == 5
assert catalan_recursif(4) == 14
assert catalan_recursif(5) == 42
