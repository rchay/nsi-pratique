def disque_est_dans_rectangle(x, y, r, x_min, y_min, x_max, y_max):
    ...



def nb_disques_dans_rectangle(rectangle, disques):
    ... = rectangle
    x_min, y_min = ...
    ... = p_B
    resultat = ...
    for x, y, r in ...:
        if disque_est_dans_rectangle(...):
            resultat = ...
    return ...



# tests

assert disque_est_dans_rectangle(
    x=30, y=53, r=1,
    x_min=10, y_min=50, x_max=80, y_max=60
), "Erreur"

assert not disque_est_dans_rectangle(
    x=30, y=53, r=10,
    x_min=10, y_min=50, x_max=80, y_max=60
), "Erreur"

assert not disque_est_dans_rectangle(
    x=0, y=53, r=1,
    x_min=10, y_min=50, x_max=80, y_max=60
), "Erreur"


assert nb_disques_dans_rectangle(((10, 50), (80, 60)), []) == 0

assert nb_disques_dans_rectangle(((10, 50), (80, 60)),
     [(30, 53, 1), (30, 53, 10), (0, 53, 1)]) == 1
