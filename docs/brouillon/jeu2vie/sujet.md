---
author: Franck Chambon
title: Jeu de la vie
---
:warning: Trop d'erreurs dans le code :warning:

C'est faux pour l'instant !!!!


# Qualifier des structures du Jeu de la vie

## Règles

Une cellule possède huit voisins, qui sont les cellules adjacentes horizontalement, verticalement et diagonalement.

À chaque itération, l'état d'une cellule est entièrement déterminé par l'état de ses huit cellules voisines, selon les règles suivantes :

- Une cellule morte possédant exactement trois cellules voisines vivantes devient vivante (elle nait).
- Une cellule vivante possédant deux ou trois cellules voisines vivantes le reste, sinon elle meurt.

## Modélisation

On utilise pour représenter une grille rectangulaire, une liste de listes d'entiers égaux à 0 ou 1.

- 0 désigne une cellule morte.
- 1 désigne une cellule vivante.

```python
MOTIF_A0 = [
    [0, 0, 0, 0, 0],
    [0, 1, 0, 0, 0],
    [0, 1, 0, 1, 0],
    [0, 0, 0, 0, 0],
]

MOTIF_A1 = [
    [0, 0, 0, 0, 0],
    [0, 0, 0, 0, 0],
    [0, 0, 1, 0, 0],
    [0, 0, 0, 0, 0],
]
```

```python
MOTIF_B0 = [
    [0, 0, 0, 0, 0],
    [0, 1, 1, 1, 0],
    [0, 1, 1, 0, 0],
    [0, 0, 0, 0, 0],
]

MOTIF_B1 = [
    [0, 0, 1, 0, 0],
    [0, 1, 0, 1, 0],
    [0, 1, 0, 1, 0],
    [0, 0, 0, 0, 0],
]
MOTIF_B2 = [
    [0, 0, 1, 0, 0],
    [0, 1, 0, 1, 0],
    [0, 1, 0, 1, 0],
    [0, 0, 0, 0, 0],
]
```

## Structures

Un motif peut être qualifié de stable, de périodique, de vaisseau ou autre.

!!! info "Motif stable"

    Un motif stable est identique à lui-même à la génération suivante.

    Exemple :

    ```python
    CARRE_STABLE = [
        [0, 0, 0, 0],
        [0, 1, 1, 0],
        [0, 1, 1, 0],
        [0, 0, 0, 0],
    ]
    ```

    TODO ajouter des figures de plusieurs motifs stables



!!! info "Motif périodique"

    Un motif périodique est identique à lui-même après plusieurs générations.

    Exemple :

    ```python
    CLIGNOTANT_0 = [
        [0, 0, 0, 0, 0],
        [0, 0, 1, 0, 0],
        [0, 0, 1, 0, 0],
        [0, 0, 1, 0, 0],
        [0, 0, 0, 0, 0],
    ]

    CLIGNOTANT_1 = [
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 1, 1, 1, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
    ]
    ```

    TODO ajouter des figures de plusieurs motifs périodiques, avec des gif

!!! info "Vaisseau"

    Un vaisseau est identique à lui-même, mais décalé, après plusieurs générations.

    ![](GameOfLife_Glider_Animation.gif)

    ```python
    PLANEUR_0 = [
        [0, 0, 0, 0, 0],
        [0, 0, 1, 0, 0],
        [0, 0, 0, 1, 0],
        [0, 1, 1, 1, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
    ]

    PLANEUR_1 = [
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 1, 0, 1, 0],
        [0, 0, 1, 1, 0],
        [0, 0, 1, 0, 0],
        [0, 0, 0, 0, 0],
    ]

    PLANEUR_2 = [
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 0, 1, 0],
        [0, 1, 0, 1, 0],
        [0, 0, 1, 1, 0],
        [0, 0, 0, 0, 0],
    ]

    PLANEUR_3 = [
        [0, 0, 0, 0, 0],
        [0, 0, 0, 0, 0],
        [0, 0, 1, 0, 0],
        [0, 1, 0, 1, 1],
        [0, 0, 1, 1, 0],
        [0, 0, 0, 0, 0],
    ]
    ```

    Autre vaisseau

    ![](Game_of_life_animated_LWSS.gif)


## Le problème

Écrire quatre fonctions :

- `successeur(motif)` doit renvoyer la transformation du motif à l'étape suivante.
- `est_stable(motif)` doit renvoyer un booléen, la réponse à la question :
    - Le `motif` est-il identique à lui-même après une génération ?
- `est_periodique(motif)` doit renvoyer un booléen, la réponse à la question :
    - Le `motif` est-il identique à lui-même après plusieurs générations ?
    - On garantit que dans les tests, moins de 20 générations suffisent à discriminer les structures périodiques.
- `est_vaisseau(motif)` doit renvoyer un booléen, la réponse à la question :
    - Le `motif` est-il identique à lui-même, mais déplacé, après plusieurs générations ?
    - On garantit que dans les tests, moins de 20 générations suffisent à discriminer les vaisseaux.

Dans tous les cas, le `motif` donné en paramètre aura au moins deux lignes et deux colonnes et sera entouré de cellules mortes. La sortie pourrait ne pas l'être, d'où l'utilisation de la fonction `encadre` qu'il faudra utiliser.

{{ IDE('exo') }}
