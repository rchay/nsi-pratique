def catalan_iteratif(n: int) -> int:
    i = 0
    C = 1
    while i != n:
        C = (4*i + 2)/(i + 2)*C
        i = i + 1
    return C
