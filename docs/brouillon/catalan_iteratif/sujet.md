---
title: Nombre de Catalan (itératif)
author: Vincent-Xavier Jumel
---

# Nombres de Catalan itératif

On définit les nombres de Catalan (nommés en l'honneur de  Eugène Charles
    Catalan (1814-1894)) de plusieurs façons. Par exemple, on peut
considérer qu'il s'agit du nombre de mots de Dyck à $2n$ lettres contenant
$n$ lettres *X* et $n$ lettres *Y* telles que dans tout préfixe, le nombre de
*X* soit supérieur où égal au nombre de *Y*.

+ `XXXYYY` est un mot de Dyck
+ `XYXXYY` est aussi un mot de Dyck
+ `XYYXXY` n'est pas un mot de Dyck

Écrire un programme **itératif** `catalan_iteratif` qui calcule ce nombre.

On peut utiliser la formule $C_{n + 1} = \frac{2(2n + 1)}{n + 2} C_n$.

!!! example "Exemples"

    ```pycon
    >>> catalan_iteratif(0)
    1
    >>> catalan_iteratif(1)
    1
    >>> catalan_iteratif(2)
    2
    >>> catalan_iteratif(3)
    5
    >>> catalan_iteratif(4)
    14
    >>> catalan_iteratif(5)
    42
    ```

{{ IDE('exo') }}
