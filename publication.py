from git import Repo
from sys import argv


repo = Repo("./")

origin = repo.remote()
cli = origin.repo.git

while True:
    try:
        cli.pull("origin", "main")
        break
    except:
        pass

cli.checkout("prod")
cli.branch("-D", "preprod")
cli.checkout("-b", "preprod")

while True:
    try:
        cli.pull("origin", "preprod")
        break
    except:
        pass

for file in argv[1:]:
    cli.checkout("main", file)
    cli.add(file)
    cli.commit("-m", f"Publication de {file}")

while True:
    try:
        cli.push("origin", "preprod")
        break
    except:
        pass

cli.checkout("main")
